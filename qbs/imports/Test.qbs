import qbs

Product {
    files: ["**"]
    type: ["application", "autotest"]

    cpp.cxxFlags: {
        if (project.enableCoverage) {
            return ["--coverage"]
        }
    }
    cpp.cxxLanguageVersion: "c++17"
    cpp.debugInformation: true
    cpp.dynamicLibraries: project.enableCoverage ? ["gcov"] : undefined
    cpp.enableExceptions: false
    cpp.includePaths: [project.sourceDirectory + '/src']

    Depends { name: 'cpp' }
    Depends { name: 'Qt.core' }
    Depends { name: 'Qt.test' }
}
