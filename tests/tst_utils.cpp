/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include <QTest>

using namespace MiTubo;

class UtilsTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testExtractUrls_data();
    void testExtractUrls();
};

void UtilsTest::testExtractUrls_data()
{
    QTest::addColumn<QString>("text");
    QTest::addColumn<QStringList>("expectedUrls");

    QTest::newRow("empty") << "" << QStringList();

    QTest::newRow("no URL") <<
        "Some text, with! punctuations.but no urls" <<
        QStringList();

    QTest::newRow("just a URL") <<
        "http://www.mardy.it/with/a/path" <<
        QStringList { "http://www.mardy.it/with/a/path" };

    QTest::newRow("text with a URL") <<
        "Go to https://example.com for more info" <<
        QStringList { "https://example.com" };

    QTest::newRow("text with more URLs") <<
        "Either https://example.com, https://www.ab.com/path&q=12&m=%20lk\n"
        "then\thttp://me.you (https://another.one/here)." <<
        QStringList {
            "https://example.com",
            "https://www.ab.com/path&q=12&m=%20lk",
            "http://me.you",
            "https://another.one/here",
        };
}

void UtilsTest::testExtractUrls()
{
    QFETCH(QString, text);
    QFETCH(QStringList, expectedUrls);

    Utils utils;
    const QStringList urls = utils.extractUrls(text);
    QCOMPARE(urls, expectedUrls);
}

QTEST_GUILESS_MAIN(UtilsTest)

#include "tst_utils.moc"
