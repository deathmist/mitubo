import qbs 1.0
import qbs.Environment
import "src/sortfilterproxymodel/SortFilterProxyModel.qbs" as SortFilterProxyModelSources
import "src/qhtmlparser/sources.qbs" as HtmlParserSources

Project {
    name: "MiTubo"

    property bool buildTests: false
    property bool enableCoverage: false

    property string packageName: "it.mardy.mitubo"
    property string packageType: "other"
    property string version: "1.5"

    qbsSearchPaths: "qbs"

    QtGuiApplication {
        name: "MiTubo"
        version: project.version
        install: true
        consoleApplication: false

        cpp.cxxLanguageVersion: "c++17"
        cpp.defines: [
            'QT_DISABLE_DEPRECATED_BEFORE=0x050900',
            'QT_DEPRECATED_WARNINGS=0',
            'APP_VERSION="' + product.version + '"',
            'PACKAGE_TYPE="' + project.packageType + '"',
        ]
        cpp.includePaths: [
            htmlParserSources.prefix,
            sortFilterProxyModelSources.prefix,
        ]
        cpp.enableExceptions: false

        qbs.installPrefix: {
            if (qbs.targetOS.contains("darwin"))
                return ""
            return original
        }

        Group {
            prefix: "src/"
            files: [
                "abstract_github_installer.cpp",
                "abstract_github_installer.h",
                "abstract_installer.h",
                "env_setup.cpp",
                "env_setup.h",
                "feed.cpp",
                "feed.h",
                "feed_discoverer.cpp",
                "feed_discoverer.h",
                "feed_item_model.cpp",
                "feed_item_model.h",
                "feed_model.cpp",
                "feed_model.h",
                "feed_parser.cpp",
                "feed_parser.h",
                "feed_store.cpp",
                "feed_store.h",
                "html_parser.cpp",
                "html_parser.h",
                "main.cpp",
                "playlist.cpp",
                "playlist.h",
                "playlist_model.cpp",
                "playlist_model.h",
                "playlist_store.cpp",
                "playlist_store.h",
                "request_dispatcher.cpp",
                "request_dispatcher.h",
                "settings.cpp",
                "settings.h",
                "types.cpp",
                "types.h",
                "updater.cpp",
                "updater.h",
                "utils.cpp",
                "utils.h",
                "video_detector.cpp",
                "video_detector.h",
                "video_extractor.cpp",
                "video_extractor.h",
                "youtube_dl.cpp",
                "youtube_dl.h",
                "youtube_dl_installer.cpp",
                "youtube_dl_installer.h",
                "yt_dlp_installer.cpp",
                "yt_dlp_installer.h",
            ]
        }

        Group {
            prefix: "src/desktop/"
            files: [
                "qml/ui.qrc",
            ]
        }

        Group {
            fileTagsFilter: "qm"
            fileTags: ["qt.core.resource_data"]
            Qt.core.resourcePrefix: "/i18n/"
        }

        Group {
            prefix: "data/"
            files: [
                "i18n/*.ts",
                "icons/icons.qrc",
                "icons/mitubo.rc",
                "it.mardy.mitubo.metainfo.xml",
                "MiTubo-Info.plist",
                "mitubo.desktop",
            ]
        }

        Group {
            files: "data/mitubo.svg"
            fileTags: "freedesktop.appIcon"
        }

        HtmlParserSources {
            id: htmlParserSources
        }

        SortFilterProxyModelSources {
            id: sortFilterProxyModelSources
        }

        Depends { name: "cpp" }
        Depends { name: "Qt.core" }
        Depends { name: "Qt.quick" }
        Depends { name: "Qt.quickcontrols2" }
        Depends { name: "Qt.svg" }
        Depends { name: "QScreenSaver" }
        Depends { name: "freedesktop" }

        freedesktop.name: product.name

        /*
         * Ubuntu Touch specific configuration
         */
        Depends {
            name: "ubuntutouch"
            condition: Environment.getEnv("TARGET_SYSTEM") == "UbuntuTouch"
        }

        Properties {
            condition: ubuntutouch.present
            freedesktop.desktopKeys: ubuntutouch.desktopKeys
            ubuntutouch.overrideDesktopKeys: ({
                "Icon": "./share/icons/hicolor/scalable/apps/mitubo.svg",
            })
            ubuntutouch.manifest: ({
                "version": project.version + "-0"
            })
        }

        Group {
            condition: ubuntutouch.present
            prefix: "data/ubuntu-touch/"
            files: [
                "manifest.json",
                "mitubo.apparmor",
                "mitubo.content-hub",
            ]
        }

        Group {
            condition: ubuntutouch.present
            prefix: "src/ubuntu-touch/"
            files: [
                "qml/ubuntu-touch.qrc",
            ]
        }

        /*
         * Windows specific configuration
         */
        Group {
            condition: qbs.targetOS.contains("windows")
            prefix: "src/desktop/windows/"
            files: [
                "qml/windows.qrc",
            ]
        }
    }

    references: [
        "src/qscreensaver/staticlibrary.qbs",
        "tests/tests.qbs",
    ]

    AutotestRunner {
        name: "check"
        Depends { productTypes: ["coverage-clean"] }
    }
}
