/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playlist.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QRegularExpression>
#include <QStringList>
#include <QUrl>
#include <algorithm>

using namespace MiTubo;

namespace MiTubo {

static const QString keyInputUrl = QStringLiteral("inputUrl");
static const QString keyWebpageUrl = QStringLiteral("webpage_url");
static const QString keyStartTime = QStringLiteral("startTime");
static const QString keyDateAdded = QStringLiteral("dateAdded");
static const QString keyUrlAliases = QStringLiteral("urlAliases");

static const QStringList s_savedKeys = {
    QStringLiteral("duration"),
    QStringLiteral("thumbnail"),
    QStringLiteral("title"),
    QStringLiteral("description"),
    QStringLiteral("uploader"),
    QStringLiteral("view_count"),
    keyDateAdded,
    keyStartTime,
    keyInputUrl,
    keyUrlAliases,
    keyWebpageUrl,
};

class PlaylistPrivate {
public:
    PlaylistPrivate(const QString &uniqueId, const QString &name,
                    bool isEditable,
                    Playlist::SortOrder order, const QJsonArray &items);

    int indexOf(const QJsonObject &videoData) const;
    int indexOf(const QString &url) const;
    void updateVideo(const QJsonObject &videoData);
    void addOrReplace(const QJsonObject &videoData, int index);
    void addOrReplace(const QJsonObject &videoData);

    static QJsonObject filterKeys(const QJsonObject &videoData);
    static QString nameOrIdTranslation(const QString &name,
                                       const QString &uniqueId);
    static QString uniqueIdFromName(const QString &name);

    static double itemTime(const QJsonValue &item);
    static bool sortOlderFirst(const QJsonValue &a, const QJsonValue &b);
    static bool sortNewerFirst(const QJsonValue &a, const QJsonValue &b);

    void updateSortOrder();

private:
    friend class Playlist;
    QString m_name;
    QString m_uniqueId;
    bool m_isEditable;
    Playlist::SortOrder m_sortOrder;
    QJsonArray m_items;
};

} // namespace

static void swap(QJsonValueRef v1, QJsonValueRef v2)
{
    QJsonValue temp(v1);
    v1 = QJsonValue(v2);
    v2 = temp;
}

PlaylistPrivate::PlaylistPrivate(const QString &uniqueId,
                                 const QString &name,
                                 bool isEditable,
                                 Playlist::SortOrder order,
                                 const QJsonArray &items):
    m_name(nameOrIdTranslation(name, uniqueId)),
    m_uniqueId(uniqueId.isEmpty() ? uniqueIdFromName(name) : uniqueId),
    m_isEditable(isEditable),
    m_sortOrder(order)
{
    for (const QJsonValue &v: items) {
        QJsonObject o = filterKeys(v.toObject());
        if (!o.isEmpty()) {
            m_items.append(o);
        }
    }

    updateSortOrder();
}

int PlaylistPrivate::indexOf(const QJsonObject &videoData) const
{
    int ret = indexOf(videoData.value(keyInputUrl).toString());
    if (ret < 0) {
        // Try with webpage URL
        ret = indexOf(videoData.value(keyWebpageUrl).toString());
    }
    return ret;
}

int PlaylistPrivate::indexOf(const QString &url) const
{
    for (auto i = m_items.begin(); i != m_items.end(); i++) {
        const QJsonObject item = i->toObject();
        if (item.value(keyInputUrl).toString() == url) {
            return i - m_items.begin();
        }

        const QJsonArray aliases = item.value(keyUrlAliases).toArray();
        for (const QJsonValue &v: aliases) {
            if (v.toString() == url) {
                return i - m_items.begin();
            }
        }
    }
    return -1;
}

void PlaylistPrivate::updateVideo(const QJsonObject &videoData)
{
    int i = indexOf(videoData);
    if (i >= 0) {
        m_items.replace(i, videoData);
    } else {
        // Or should we just insert a new item?
        qWarning() << "Cannot update unexisting video";
    }
}

void PlaylistPrivate::addOrReplace(const QJsonObject &videoData, int index)
{
    int i = indexOf(videoData);
    if (i >= 0) {
        m_items.removeAt(i);
    }

    /* If the video was removed, the index might not be valid anymore; make
     * sure it's within the valid range. */
    index = qMin(index, m_items.size());
    m_items.insert(index, videoData);
}

void PlaylistPrivate::addOrReplace(const QJsonObject &videoData)
{
    addOrReplace(videoData,
                 m_sortOrder == Playlist::NewerFirst ? 0 : m_items.size());
}

QJsonObject PlaylistPrivate::filterKeys(const QJsonObject &videoData)
{
    QJsonObject video;
    for (auto i = videoData.begin(); i != videoData.end(); i++) {
        if (s_savedKeys.contains(i.key())) {
            video.insert(i.key(), i.value());
        }
    }
    return video;
}

QString PlaylistPrivate::nameOrIdTranslation(const QString &name,
                                             const QString &uniqueId)
{
    QString idTranslation =
        QCoreApplication::translate("MiTubo::Playlist",
                                    uniqueId.toUtf8().constData());
    if (!idTranslation.isEmpty() && idTranslation != uniqueId) {
        return idTranslation;
    } else {
        return name.isEmpty() ? uniqueId : name;
    }
}

QString PlaylistPrivate::uniqueIdFromName(const QString &name)
{
    QRegularExpression re("[\\/|.:\"*?]");
    QString uniqueId = name;
    return uniqueId.replace(re, "_");
}

double PlaylistPrivate::itemTime(const QJsonValue &item)
{
    return item.toObject().value(keyDateAdded).toDouble();
}

bool PlaylistPrivate::sortOlderFirst(const QJsonValue &a, const QJsonValue &b)
{
    return itemTime(a) < itemTime(b);
}

bool PlaylistPrivate::sortNewerFirst(const QJsonValue &a, const QJsonValue &b)
{
    return itemTime(a) > itemTime(b);
}

void PlaylistPrivate::updateSortOrder()
{
    if (!m_isEditable) return;

    auto compareFunc = m_sortOrder == Playlist::OlderFirst ?
        PlaylistPrivate::sortOlderFirst :
        PlaylistPrivate::sortNewerFirst;
    std::sort(m_items.begin(), m_items.end(), compareFunc);
}

Playlist::Playlist(const QString &name,
                   const QJsonArray &items,
                   bool isEditable,
                   QObject *parent):
    QObject(parent),
    d_ptr(new PlaylistPrivate(QString(), name, isEditable, NewerFirst, items))
{
}

Playlist::Playlist(const QString &uniqueId, const QString &name,
                   SortOrder order, const QJsonArray &items,
                   QObject *parent):
    QObject(parent),
    d_ptr(new PlaylistPrivate(uniqueId, name, true, order, items))
{
}

Playlist *Playlist::fromDownloader(const QJsonObject &playlistData,
                                   QObject *parent)
{
    if (Q_UNLIKELY(!playlistData.value(QLatin1String("isPlaylist")).
                   toBool())) {
        qWarning() << "Playlist::fromDownloader called on non playlist";
        return nullptr;
    }

    const QString name =
        playlistData.value(QLatin1String("name")).toString();
    const QJsonArray items =
        playlistData.value(QLatin1String("items")).toArray();
    return new Playlist(name, items, false, parent);
}

Playlist::~Playlist() = default;

QString Playlist::name() const
{
    Q_D(const Playlist);
    return d->m_name;
}

QString Playlist::uniqueId() const
{
    Q_D(const Playlist);
    return d->m_uniqueId;
}

bool Playlist::isEditable() const
{
    Q_D(const Playlist);
    return d->m_isEditable;
}

void Playlist::setSortOrder(SortOrder order)
{
    Q_D(Playlist);
    if (order == d->m_sortOrder) return;
    d->m_sortOrder = order;

    d->updateSortOrder();
    Q_EMIT itemsChanged();
}

Playlist::SortOrder Playlist::sortOrder() const
{
    Q_D(const Playlist);
    return d->m_sortOrder;
}

QJsonArray Playlist::items() const
{
    Q_D(const Playlist);
    return d->m_items;
}

int Playlist::count() const
{
    Q_D(const Playlist);
    return d->m_items.count();
}

void Playlist::addVideo(const QUrl &url, qint64 position)
{
    addVideo({
        { keyInputUrl, url.toString() },
    }, position);
}

void Playlist::addVideo(const QJsonObject &videoData, qint64 position)
{
    Q_D(Playlist);

    if (!d->m_isEditable) {
        qWarning() << "Adding videos to uneditable playlist";
        return;
    }

    QJsonObject video = PlaylistPrivate::filterKeys(videoData);
    if (!video.isEmpty()) {
        video.insert(keyStartTime, position);
        video.insert(keyDateAdded, QDateTime::currentSecsSinceEpoch());
        d->addOrReplace(video);
        Q_EMIT itemsChanged();
    }
}

void Playlist::updateVideo(const QJsonObject &videoData)
{
    Q_D(Playlist);

    if (!d->m_isEditable) {
        qWarning() << "Updating video in uneditable playlist";
        return;
    }

    d->updateVideo(videoData);
    Q_EMIT itemsChanged();
}

void Playlist::addVideos(const QJsonArray &videos, int index)
{
    Q_D(Playlist);

    if (!d->m_isEditable) {
        qWarning() << "Adding videos to uneditable playlist";
        return;
    }

    for (const QJsonValue &v: videos) {
        QJsonObject videoData;
        if (v.isString()) {
            videoData.insert(keyInputUrl, v.toString());
        } else if (v.isObject()) {
            videoData = PlaylistPrivate::filterKeys(v.toObject());
        } else {
            qWarning() << "Unsupported video type:" << v.type();
            continue;
        }
        videoData.insert(keyDateAdded, QDateTime::currentSecsSinceEpoch());
        d->addOrReplace(videoData, index++);
    }
    Q_EMIT itemsChanged();
}

void Playlist::addVideos(const QStringList &urls, int index)
{
    addVideos(QJsonArray::fromStringList(urls), index);
}

int Playlist::indexOf(const QJsonObject &videoData) const
{
    Q_D(const Playlist);
    return d->indexOf(videoData);
}

int Playlist::indexOf(const QUrl &url) const
{
    Q_D(const Playlist);
    return d->indexOf(url.toString());
}

QJsonObject Playlist::itemAt(int index) const
{
    Q_D(const Playlist);
    return d->m_items.at(index).toObject();
}

QJsonObject Playlist::takeAt(int index)
{
    Q_D(Playlist);
    if (!d->m_isEditable) {
        qWarning() << "Removing object from uneditable playlist";
        return QJsonObject();
    }
    QJsonObject ret = d->m_items.takeAt(index).toObject();
    Q_EMIT itemsChanged();
    return ret;
}

QString Playlist::continueWatchingListId() {
    return QStringLiteral(QT_TR_NOOP("ContinueWatching"));
}

QString Playlist::watchHistoryListId() {
    return QStringLiteral(QT_TR_NOOP("WatchHistory"));
}

QString Playlist::watchLaterListId() {
    return QStringLiteral(QT_TR_NOOP("WatchLater"));
}
