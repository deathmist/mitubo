/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "video_detector.h"

#include <Html/Parser>
#include <QByteArray>
#include <QDateTime>
#include <QDebug>
#include <QIODevice>
#include <QJsonArray>
#include <QJsonValue>

using namespace MiTubo;
using namespace it::mardy::Html;

namespace MiTubo {

class VideoDetectorPrivate: public Parser
{
    enum State {
        NotStarted = 0,
        InHtml,
        InHead,
        InBody,
    };

    struct CurrentInfo {
        QString videoUrl;
        QString title;
        QString description;
        QString thumbnailUrl;
        int width = -1;
        int height = -1;
        qreal duration = 0.0;
        int viewCount = -1;
        QDateTime uploadDate;
    };

public:
    VideoDetectorPrivate(VideoDetector *q);

    void handleStartTag(const QString &tag, const Parser::Attributes &attrs) override;
    void handleEndTag(const QString &tag) override;
    void handleIframe(const Parser::Attributes &attrs);
    void handleEndHead();

private:
    Q_DECLARE_PUBLIC(VideoDetector)
    State m_state;
    QUrl m_baseUrl;
    CurrentInfo m_info;
    QVector<VideoDetector::DetectedVideo> m_detectedVideos;
    VideoDetector *q_ptr;
};

} // namespace

VideoDetectorPrivate::VideoDetectorPrivate(VideoDetector *q):
    Parser(Parser::ConvertCharRefs),
    m_state(NotStarted),
    q_ptr(q)
{
    m_detectedVideos.reserve(10);
}

void VideoDetectorPrivate::handleStartTag(const QString &tag,
                                          const Parser::Attributes &attrs)
{
    if (m_state == NotStarted) {
        if (tag == "html") {
            m_state = InHtml;
        }
    } else if (m_state == InHtml) {
        if (tag == "head") {
            m_state = InHead;
            m_info = {};
        } else if (tag == "body") {
            m_state = InBody;
        }
    } else if (m_state == InBody) {
        if (tag == "iframe") {
            handleIframe(attrs);
        }
    } else if (m_state == InHead) {
        /* Check if there's the Yandex-specific URL for the video */
        if (tag == "meta") {
            QString property = attrs.value("property");
            if (property == "ya:ovs:content_url") {
                m_info.videoUrl = attrs.value("content");
            } else if (property == "og:duration") {
                m_info.duration = attrs.value("content").toDouble();
            } else if (property == "og:video:height") {
                m_info.height = attrs.value("content").toInt();
            } else if (property == "og:video:width") {
                m_info.width = attrs.value("content").toInt();
            } else if (property == "ya:ovs:views_total") {
                m_info.viewCount = attrs.value("content").toInt();
            } else if (property == "og:title") {
                m_info.title = attrs.value("content");
            } else if (property == "og:description") {
                m_info.description = attrs.value("content");
            } else if (property == "og:image") {
                m_info.thumbnailUrl = attrs.value("content");
            } else if (property == "ya:ovs:upload_date") {
                m_info.uploadDate =
                    QDateTime::fromString(attrs.value("content"), Qt::ISODate);
            }
        }
    }
}

void VideoDetectorPrivate::handleEndTag(const QString &tag)
{
    Q_Q(VideoDetector);
    if (m_state == InHead) {
        if (tag == "head") {
            handleEndHead();
            m_state = InHtml;
        } else if (tag == "body") {
            Q_EMIT q->finished();
        }
    }
}

void VideoDetectorPrivate::handleIframe(const Parser::Attributes &attrs)
{
    for (const Attribute &attr: attrs) {
        if (attr.name.startsWith("data-src")) {
            QUrl url = attr.value;
            // Assume YouTube
            m_detectedVideos.append({
                url,
                QJsonObject(),
                VideoDetector::LinkYoutube,
            });
        }
    }
}

void VideoDetectorPrivate::handleEndHead()
{
    if (!m_info.videoUrl.isEmpty()) {
        const QJsonValue null;
        QJsonObject format {
            { "acodec", "unknown" },
            { "vcodec", "unknown" },
            { "width", m_info.width >= 0 ? m_info.width : null },
            { "height", m_info.height >= 0 ? m_info.width : null },
            { "url", m_info.videoUrl },
        };
        QJsonObject videoInfo {
            { "formats", QJsonArray { format }},
            { "title", m_info.title },
            { "description", m_info.description },
            { "thumbnail", m_info.thumbnailUrl },
            { "duration", m_info.duration >= 0 ? m_info.duration : null },
            { "view_count", m_info.viewCount >= 0 ? m_info.viewCount : null },
            { "upload_date", m_info.uploadDate.isNull() ? null :
                m_info.uploadDate.toString("yyyyMMdd") },
            { "webpage_url", m_baseUrl.toString() },
        };
        m_detectedVideos.append({
            QUrl(),
            videoInfo,
            VideoDetector::LinkMediaFile,
        });
    }
}

VideoDetector::VideoDetector(QObject *parent):
    QObject(parent),
    d_ptr(new VideoDetectorPrivate(this))
{
}

VideoDetector::~VideoDetector() = default;

void VideoDetector::reset()
{
    Q_D(VideoDetector);
    d->m_state = VideoDetectorPrivate::NotStarted;
    d->m_baseUrl.clear();
    d->m_detectedVideos.clear();
}

void VideoDetector::setBaseUrl(const QUrl &url)
{
    Q_D(VideoDetector);
    d->m_baseUrl = url;
}

void VideoDetector::feed(const QByteArray &data)
{
    Q_D(VideoDetector);
    d->feed(QString::fromUtf8(data));
}

const QVector<VideoDetector::DetectedVideo> &VideoDetector::videosFound() const
{
    Q_D(const VideoDetector);
    return d->m_detectedVideos;
}
