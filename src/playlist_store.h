/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_PLAYLIST_STORE_H
#define MITUBO_PLAYLIST_STORE_H

#include <QObject>
#include <QScopedPointer>
#include <QSharedPointer>
#include <QString>

namespace MiTubo {

class Playlist;

class PlaylistStorePrivate;
class PlaylistStore: public QObject
{
    Q_OBJECT

public:
    ~PlaylistStore();

    static PlaylistStore *instance();

    QList<QSharedPointer<Playlist>> playlists() const;

    /* Takes ownership of playlist */
    void insertPlaylist(Playlist *playlist, int position = -1);
    void deletePlaylist(const QString &name);

Q_SIGNALS:
    /* Only emitted when playlists are added or removed */
    void playlistsChanged();

protected:
    PlaylistStore(QObject *parent = nullptr);

private:
    Q_DECLARE_PRIVATE(PlaylistStore)
    QScopedPointer<PlaylistStorePrivate> d_ptr;
};

} // namespace

#endif // MITUBO_PLAYLIST_STORE_H
