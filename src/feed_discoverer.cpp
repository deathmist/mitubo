/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "feed_discoverer.h"

#include "html_parser.h"

#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QRegularExpression>
#include <QTimer>
#include <QUrl>

using namespace MiTubo;

namespace {

struct Feed {
    QUrl url;
    QString title;
    QString mimeType;
};

} // namespace

namespace MiTubo {

class FeedDiscovererPrivate {
public:
    FeedDiscovererPrivate(FeedDiscoverer *q);

    void clear();
    void addFeed(const Feed &feed);
    bool checkReplyIsFeed();
    void probeUrl(const QUrl &url);

private:
    Q_DECLARE_PUBLIC(FeedDiscoverer)
    QVector<Feed> m_feeds;
    QHash<int, QByteArray> m_roles;
    QString m_inputText;
    QUrl m_canonicalUrl;
    QRegularExpression m_ytChannelRegexp;
    QNetworkAccessManager m_nam;
    QScopedPointer<QNetworkReply> m_reply;
    QTimer m_probeTimer;
    HtmlParser m_htmlParser;
    FeedDiscoverer *q_ptr;
};

} // namespace

FeedDiscovererPrivate::FeedDiscovererPrivate(FeedDiscoverer *q):
    m_ytChannelRegexp(R"(https?://(?:www.)?youtube.com/channel/(\w*))"),
    q_ptr(q)
{
    m_roles[FeedDiscoverer::UrlRole] = "url";
    m_roles[FeedDiscoverer::TitleRole] = "title";
    m_roles[FeedDiscoverer::MimeTypeRole] = "mimeType";

    m_probeTimer.setSingleShot(true);
    m_probeTimer.setInterval(500);
    QObject::connect(&m_probeTimer, &QTimer::timeout,
                     q, [this]() {
        probeUrl(QUrl::fromUserInput(m_inputText));
    });

    QObject::connect(&m_htmlParser, &HtmlParser::gotAlternate,
                     q, [this](const QString &title,
                               const QString &type,
                               const QUrl &url) {
        if (type == "application/rss+xml" ||
            type == "application/atom+xml") {
            addFeed({ url, title, type });
        }
    });
    QObject::connect(&m_htmlParser, &HtmlParser::finished,
                     q, [this]() {
        QNetworkReply *reply = m_reply.take();
        reply->deleteLater();
    });
}

bool FeedDiscovererPrivate::checkReplyIsFeed()
{
    Q_Q(FeedDiscoverer);
    /* Is this URL itself a RSS/Atom feed? */
    const QString contentTypeHeader =
        m_reply->header(QNetworkRequest::ContentTypeHeader).toString();
    /* There might be an encoding specified: strip it */
    const QString mimeType =
        contentTypeHeader.left(contentTypeHeader.indexOf(';'));
    if (mimeType == "application/atom+xml" ||
        mimeType == "application/rss+xml" ||
        mimeType == "application/xml") {
        QNetworkReply *reply = m_reply.take();
        reply->deleteLater();
        m_canonicalUrl = reply->url();
        qDebug() << "Feed" << mimeType << "detected at" << m_canonicalUrl;
        Q_EMIT q->isFeedChanged();
        return true;
    }
    return false;
}

void FeedDiscovererPrivate::probeUrl(const QUrl &url)
{
    Q_Q(FeedDiscoverer);

    m_htmlParser.reset();

    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
                     QNetworkRequest::NoLessSafeRedirectPolicy);
    m_reply.reset(m_nam.get(req));
    QObject::connect(m_reply.data(), &QIODevice::readyRead,
                     q, [this]() {
        if (!m_reply) return;
        int statusCode =
            m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).
            toInt();
        if (statusCode == 0) return;
        if (statusCode < 200 || statusCode >= 300) {
            qDebug() << "Aborting because of status code" << statusCode;
            QNetworkReply *reply = m_reply.take();
            reply->deleteLater();
            return;
        }

        if (checkReplyIsFeed()) {
            return;
        }
        QByteArray data = m_reply->readAll();
        m_htmlParser.setBaseUrl(m_reply->url());
        m_htmlParser.feed(data);
    });
    QObject::connect(m_reply.data(), &QNetworkReply::finished,
                     q, [this]() {
        if (!m_reply) return;
        m_reply->deleteLater();
        m_reply.take();
    });
    using NetErr = QNetworkReply::NetworkError;
    QObject::connect(m_reply.data(),
                     QOverload<NetErr>::of(&QNetworkReply::error),
                     q, [](NetErr code) {
        qWarning() << "Network error:" << code;
    });
}

void FeedDiscovererPrivate::clear()
{
    Q_Q(FeedDiscoverer);
    q->beginResetModel();
    m_feeds.clear();
    q->endResetModel();
}

void FeedDiscovererPrivate::addFeed(const Feed &feed)
{
    Q_Q(FeedDiscoverer);

    int row = m_feeds.count();

    q->beginInsertRows(QModelIndex(), row, row);
    m_feeds.append(feed);
    q->endInsertRows();
}

FeedDiscoverer::FeedDiscoverer(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new FeedDiscovererPrivate(this))
{
}

FeedDiscoverer::~FeedDiscoverer() = default;

void FeedDiscoverer::setInputText(const QString &text)
{
    Q_D(FeedDiscoverer);

    if (d->m_inputText == text) return;
    d->m_inputText = text;
    Q_EMIT inputTextChanged();
    d->m_canonicalUrl.clear();
    Q_EMIT isFeedChanged();

    d->clear();
    /* TODO: support retrieving the HTML and get alternate RSS URLs from the
     * HEAD */

    QRegularExpressionMatch match = d->m_ytChannelRegexp.match(text);
    if (match.hasMatch()) {
        QString channelId = match.captured(1);
        d->addFeed({
            "https://www.youtube.com/feeds/videos.xml?channel_id=" + channelId,
            "YouTube",
            "application/atom+xml"
        });
    } else {
        d->m_probeTimer.start();
    }
}

QString FeedDiscoverer::inputText() const
{
    Q_D(const FeedDiscoverer);
    return d->m_inputText;
}

bool FeedDiscoverer::isFeed() const
{
    Q_D(const FeedDiscoverer);
    return !d->m_canonicalUrl.isEmpty();
}

QUrl FeedDiscoverer::canonicalUrl() const
{
    Q_D(const FeedDiscoverer);
    return d->m_canonicalUrl;
}

QVariant FeedDiscoverer::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

QVariant FeedDiscoverer::data(const QModelIndex &index, int role) const
{
    Q_D(const FeedDiscoverer);

    Q_ASSERT(index.column() == 0);

    int row = index.row();
    if (Q_UNLIKELY(row < 0 || row >= rowCount())) {
        qWarning() << "Invalid index requested:" << row;
        return QVariant();
    }

    const Feed &feed = d->m_feeds[row];
    switch (role) {
    case UrlRole:
        return feed.url;
    case TitleRole:
        return feed.title;
    case MimeTypeRole:
        return feed.mimeType;
    default:
        return QVariant();
    }
}

int FeedDiscoverer::rowCount(const QModelIndex &parent) const
{
    Q_D(const FeedDiscoverer);
    Q_ASSERT(!parent.isValid());
    return d->m_feeds.count();
}

QHash<int, QByteArray> FeedDiscoverer::roleNames() const
{
    Q_D(const FeedDiscoverer);
    return d->m_roles;
}
