/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_FEED_H
#define MITUBO_FEED_H

#include <QDateTime>
#include <QJsonObject>
#include <QString>
#include <QUrl>
#include <QVariantMap>

namespace MiTubo {

struct Feed {
    /* Url will be empty for folders */
    QUrl url;
    QString title;
    QString description;
    QUrl logoUrl;
    QDateTime lastUpdated;

    bool isFolder() const { return url.isEmpty(); }

    QJsonObject toJson() const;
    static bool fromJson(const QJsonObject &o, Feed *feed);

    QVariantMap toMap() const;
    static bool fromMap(const QVariantMap &o, Feed *feed);

    static QString subPath(const QString &parent, const QString &path);
    static QString parentPath(const QString &path);

    static const QString keyDescription;
    static const QString keyLastUpdated;
    static const QString keyLogoUrl;
    static const QString keyTitle;
    static const QString keyUrl;
};

} // namespace

#endif // MITUBO_FEED_H
