/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_PLAYLIST_MODEL_H
#define MITUBO_PLAYLIST_MODEL_H

#include <QAbstractListModel>
#include <QScopedPointer>
#include <QString>

class QUrl;

namespace MiTubo {

class Playlist;

class PlaylistModelPrivate;
class PlaylistModel: public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(QString continueWatchingListId READ continueWatchingListId
               CONSTANT)
    Q_PROPERTY(QString watchHistoryListId READ watchHistoryListId CONSTANT)
    Q_PROPERTY(QString watchLaterListId READ watchLaterListId CONSTANT)

public:
    enum Roles {
        UniqueId = Qt::UserRole,
        NameRole,
        PlaylistRole,
        CountRole,
    };
    Q_ENUM(Roles)

    PlaylistModel(QObject *parent = nullptr);
    ~PlaylistModel();

    QString continueWatchingListId() const;
    QString watchHistoryListId() const;
    QString watchLaterListId() const;

    QVariant get(int row, const QString &roleName) const;

    Q_INVOKABLE MiTubo::Playlist *playlistById(const QString &id) const;

    // This method could be moved to another class, it does not use the model
    Q_INVOKABLE MiTubo::Playlist *playlistFromDownloader(
        const QJsonObject &jsonData);

    Q_INVOKABLE MiTubo::Playlist *createPlaylist(const QString &name,
                                                 const QJsonArray &items);
    Q_INVOKABLE void deletePlaylist(int row);

    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;

Q_SIGNALS:
    void countChanged();

private:
    Q_DECLARE_PRIVATE(PlaylistModel)
    QScopedPointer<PlaylistModelPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_PLAYLIST_MODEL_H
