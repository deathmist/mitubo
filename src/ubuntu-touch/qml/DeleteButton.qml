import QtQuick.Controls 2.12

ToolButton {
	icon {
		name: "edit-delete"
		color: "red"
	}
	display: AbstractButton.TextUnderIcon
	text: qsTr("Delete")
}
