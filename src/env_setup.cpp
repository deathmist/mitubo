/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "env_setup.h"

#include <QByteArray>
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>

void MiTubo::setupEnvironment()
{
#ifdef Q_OS_DARWIN
    QString cachePath =
        QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    qputenv("PYTHONPYCACHEPREFIX", cachePath.toUtf8());
    QString appDir = QCoreApplication::applicationDirPath();
    qputenv("PATH", appDir.toUtf8() + ":" + qgetenv("PATH"));
    QDir pythonBase(appDir);
    pythonBase.cd("../Frameworks/lib/python3");
    QByteArray pythonPath = pythonBase.canonicalPath().toUtf8();
    qputenv("PYTHONPATH",  pythonPath + ":" + pythonPath + "/lib-dynload");
#endif
}
