/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_FEED_PARSER_H
#define MITUBO_FEED_PARSER_H

#include <QDateTime>
#include <QObject>
#include <QScopedPointer>
#include <QString>
#include <QUrl>

#include <functional>

class QIODevice;

namespace MiTubo {

struct FeedItem {
    QString title;
    QString description;
    Qt::TextFormat descriptionFormat = Qt::PlainText;
    QDateTime date;
    QUrl url;
    QUrl thumbnail;

    bool operator==(const FeedItem &o) const {
        return title == o.title &&
            description == o.description &&
            descriptionFormat == o.descriptionFormat &&
            date == o.date &&
            url == o.url &&
            thumbnail == o.thumbnail;
    }
};

class AbstractParser {
public:
    AbstractParser(QIODevice *) {}
    virtual ~AbstractParser() = default;

    virtual void onReadyRead() = 0;

    std::function<void(const QString &)> setTitle;
    std::function<void(const QString &description)> setDescription;
    std::function<void(const QDateTime &lastUpdate)> setLastUpdate;
    std::function<void(const QUrl &logoUrl)> setLogoUrl;
    std::function<void(const FeedItem &item)> addItem;
    std::function<void(const QString &message)> gotError;
};

class FeedParser: public QObject {
    Q_OBJECT

public:
    FeedParser(QIODevice *device);

private Q_SLOTS:
    void onReadyRead();

Q_SIGNALS:
    void gotTitle(const QString &title);
    void gotDescription(const QString &description);
    void gotLastUpdate(const QDateTime &lastUpdate);
    void gotLogoUrl(const QUrl &logoUrl);
    void gotItem(const FeedItem &item);
    void gotError(const QString &message);

private:
    QScopedPointer<AbstractParser> m_parser;
};

} // namespace

Q_DECLARE_METATYPE(MiTubo::FeedItem)

#endif // MITUBO_FEED_PARSER_H
