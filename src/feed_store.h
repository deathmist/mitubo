/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_FEED_STORE_H
#define MITUBO_FEED_STORE_H

#include "feed.h"

#include <QDateTime>
#include <QObject>
#include <QScopedPointer>
#include <QString>
#include <QUrl>

namespace MiTubo {

class FeedStorePrivate;
class FeedStore: public QObject
{
    Q_OBJECT

public:
    struct Position {
        /* This uses "/" to separate elements, therefore the "/" is not allowed
         * in feed titles. */
        QString path;
        int index = -1;
    };

    ~FeedStore();

    static FeedStore *instance();

    const QVector<Feed> feeds(const QString &path) const;
    Position findFeed(const Feed &feed) const;
    Position findFolder(const QString &path) const;
    const Feed &feedAt(const Position &pos) const;

    void addOrUpdateFeed(const Feed &feed);
    void moveFeed(const Position &from, const Position &to);
    void deleteFeed(const Position &pos);

Q_SIGNALS:
    void feedChanged(const Position &pos);
    void feedAdded(const Position &pos);
    void feedMoved(const Position &from, const Position &to);
    void feedRemoved(const Position &pos, const Feed &feed);

protected:
    FeedStore(QObject *parent = nullptr);

private:
    Q_DECLARE_PRIVATE(FeedStore)
    QScopedPointer<FeedStorePrivate> d_ptr;
};

} // namespace

#endif // MITUBO_FEED_STORE_H
