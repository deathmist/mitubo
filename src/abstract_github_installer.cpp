/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "abstract_github_installer.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

using namespace MiTubo;

AbstractGithubInstaller::AbstractGithubInstaller(const QString &projectPath):
    AbstractInstaller()
{
    m_versionUrl =
        QUrl(QStringLiteral("https://api.github.com/repos/") + projectPath +
             QStringLiteral("/releases/latest"));
}

QString AbstractGithubInstaller::parseLatestVersion(const QByteArray &contents)
{
    QJsonObject reply = QJsonDocument::fromJson(contents).object();
    QString version = reply.value(QStringLiteral("tag_name")).toString();
    // obtain download URL
    const QJsonArray assets = reply.value(QStringLiteral("assets")).toArray();
    for (const QJsonValue v: assets) {
        const QJsonObject asset = v.toObject();
        if (asset[QStringLiteral("name")] == m_programName) {
            m_downloadUrl =
                QUrl(asset[QStringLiteral("browser_download_url")].toString());
            break;
        }
    }
    return version;
}
