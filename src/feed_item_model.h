/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_FEED_ITEM_MODEL_H
#define MITUBO_FEED_ITEM_MODEL_H

#include <QAbstractListModel>
#include <QScopedPointer>
#include <QString>
#include <QUrl>
#include <QVariantMap>

namespace MiTubo {

class FeedItemModelPrivate;
class FeedItemModel: public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QVariantMap feed READ feedMap NOTIFY feedChanged)
    Q_PROPERTY(bool loading READ isLoading NOTIFY isLoadingChanged)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)

public:
    enum Roles {
        TitleRole = Qt::UserRole,
        DescriptionRole,
        DescriptionFormatRole,
        DateRole,
        UrlRole,
        ThumbnailRole,
    };
    Q_ENUM(Roles)

    FeedItemModel(QObject *parent = nullptr);
    ~FeedItemModel();

    void setUrl(const QUrl &url);
    QUrl url() const;

    bool isLoading() const;

    QVariantMap feedMap() const;

    Q_INVOKABLE void refresh();

    QVariant get(int row, const QString &roleName) const;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;

Q_SIGNALS:
    void urlChanged();
    void feedChanged();
    void isLoadingChanged();

    void countChanged();

private:
    Q_DECLARE_PRIVATE(FeedItemModel)
    QScopedPointer<FeedItemModelPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_FEED_ITEM_MODEL_H
