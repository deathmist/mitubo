/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_VIDEO_DETECTOR_H
#define MITUBO_VIDEO_DETECTOR_H

#include <QJsonObject>
#include <QObject>
#include <QScopedPointer>
#include <QString>
#include <QUrl>
#include <QVector>

class QByteArray;

namespace MiTubo {

class VideoDetectorPrivate;
class VideoDetector: public QObject
{
    Q_OBJECT

public:
    enum LinkType {
        LinkMediaFile = 0,
        LinkYoutube,  // or any link which needs youtube-dl extraction
    };
    struct DetectedVideo {
        QUrl url;
        QJsonObject info; // Has precedence over `url` if set
        LinkType linkType;
    };

    VideoDetector(QObject *parent = nullptr);
    virtual ~VideoDetector();

    void reset();
    void setBaseUrl(const QUrl &url);
    void feed(const QByteArray &data);

    const QVector<DetectedVideo> &videosFound() const;

Q_SIGNALS:
    void finished();

private:
    Q_DECLARE_PRIVATE(VideoDetector)
    QScopedPointer<VideoDetectorPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_VIDEO_DETECTOR_H
