/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "feed.h"

#include <QDebug>
#include <QJsonObject>
#include <QJsonValue>

using namespace MiTubo;

namespace MiTubo {

const QString Feed::keyDescription = QStringLiteral("description");
const QString Feed::keyLastUpdated = QStringLiteral("lastUpdated");
const QString Feed::keyLogoUrl = QStringLiteral("logoUrl");
const QString Feed::keyTitle = QStringLiteral("title");
const QString Feed::keyUrl = QStringLiteral("url");

} // namespace

QJsonObject Feed::toJson() const
{
    return {
        { keyUrl, url.toString() },
        { keyLastUpdated, lastUpdated.toString(Qt::ISODateWithMs) },
        { keyTitle, title },
        { keyDescription, description },
        { keyLogoUrl, logoUrl.toString() },
    };
}

bool Feed::fromJson(const QJsonObject &o, Feed *feed)
{
    feed->url = QUrl(o[keyUrl].toString());
    feed->lastUpdated = QDateTime::fromString(o[keyLastUpdated].toString(),
                                              Qt::ISODateWithMs);
    feed->title = o[keyTitle].toString();
    feed->description = o[keyDescription].toString();
    feed->logoUrl = QUrl(o[keyLogoUrl].toString());
    return feed->url.isValid();
}

QVariantMap Feed::toMap() const
{
    return {
        { keyUrl, url },
        { keyLastUpdated, lastUpdated },
        { keyTitle, title },
        { keyDescription, description },
        { keyLogoUrl, logoUrl },
    };
}

bool Feed::fromMap(const QVariantMap &o, Feed *feed)
{
    feed->url = QUrl(o[keyUrl].toUrl());
    feed->lastUpdated = o[keyLastUpdated].toDateTime();
    feed->title = o[keyTitle].toString();
    feed->description = o[keyDescription].toString();
    feed->logoUrl = QUrl(o[keyLogoUrl].toUrl());
    return feed->url.isValid() || !feed->title.isEmpty();
}

QString Feed::subPath(const QString &parent, const QString &path)
{
    return parent.isEmpty() ? path : (parent + '/' + path);
}

QString Feed::parentPath(const QString &path)
{
    int slash = path.lastIndexOf('/');
    if (slash < 0) {
        return QString();
    } else {
        return path.left(slash);
    }
}
