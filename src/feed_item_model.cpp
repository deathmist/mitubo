/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "feed_item_model.h"

#include "feed.h"
#include "feed_parser.h"

#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QQmlEngine>
#include <QTimer>

using namespace MiTubo;

namespace MiTubo {

class FeedItemModelPrivate {
public:
    FeedItemModelPrivate(FeedItemModel *q);

    void updateFeed();

    void queueFeedChanged();

private:
    Q_DECLARE_PUBLIC(FeedItemModel)
    Feed m_feed;

    QVector<FeedItem> m_items;

    QNetworkAccessManager m_nam;
    QScopedPointer<QNetworkReply> m_reply;
    QScopedPointer<FeedParser> m_parser;
    QHash<int, QByteArray> m_roles;
    bool m_feedChangedQueued;
    FeedItemModel *q_ptr;
};

} // namespace

FeedItemModelPrivate::FeedItemModelPrivate(FeedItemModel *q):
    m_feedChangedQueued(false),
    q_ptr(q)
{
    m_roles[FeedItemModel::TitleRole] = "title";
    m_roles[FeedItemModel::DescriptionRole] = "description";
    m_roles[FeedItemModel::DescriptionFormatRole] = "descriptionFormat";
    m_roles[FeedItemModel::DateRole] = "date";
    m_roles[FeedItemModel::UrlRole] = "url";
    m_roles[FeedItemModel::ThumbnailRole] = "thumbnail";
}

void FeedItemModelPrivate::updateFeed()
{
    Q_Q(FeedItemModel);

    m_parser.reset();

    QNetworkRequest req(m_feed.url);
    req.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
                     QNetworkRequest::NoLessSafeRedirectPolicy);
    m_reply.reset(m_nam.get(req));
    Q_EMIT q->isLoadingChanged();
    q->beginResetModel();
    m_items.clear();
    q->endResetModel();

    /* Abort the parser if the request is not successful */
    QObject::connect(m_reply.data(), &QIODevice::readyRead,
                     q, [this, q]() {
        int statusCode =
            m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).
            toInt();
        if (statusCode < 200 || statusCode >= 300) {
            qDebug() << "Aborting because of status code" << statusCode;
            m_parser.reset();
            QNetworkReply *reply = m_reply.take();
            QObject::disconnect(reply, 0, q, 0);
            reply->deleteLater();
            Q_EMIT q->isLoadingChanged();
        }
    });
    QObject::connect(m_reply.data(), &QNetworkReply::finished,
                     q, [this, q]() {
        m_parser.reset();
        QNetworkReply *reply = m_reply.take();
        if (reply) {
            QObject::disconnect(reply, 0, q, 0);
            reply->deleteLater();
        }
        Q_EMIT q->isLoadingChanged();
    });

    m_parser.reset(new FeedParser(m_reply.data()));
    QObject::connect(m_parser.data(), &FeedParser::gotTitle,
                     q, [this](const QString &title) {
        m_feed.title = title;
        queueFeedChanged();
    });
    QObject::connect(m_parser.data(), &FeedParser::gotDescription,
                     q, [this](const QString &description) {
        m_feed.description = description;
        queueFeedChanged();
    });
    QObject::connect(m_parser.data(), &FeedParser::gotLastUpdate,
                     q, [this](const QDateTime &lastUpdate) {
        m_feed.lastUpdated = lastUpdate;
        queueFeedChanged();
    });
    QObject::connect(m_parser.data(), &FeedParser::gotLogoUrl,
                     q, [this](const QUrl &logoUrl) {
        m_feed.logoUrl = logoUrl;
        queueFeedChanged();
    });
    QObject::connect(m_parser.data(), &FeedParser::gotItem,
                     q, [this, q](const FeedItem &item) {
        int row = m_items.count();
        q->beginInsertRows(QModelIndex(), row, row);
        m_items.append(item);
        q->endInsertRows();
    });
}

void FeedItemModelPrivate::queueFeedChanged()
{
    Q_Q(FeedItemModel);

    if (m_feedChangedQueued) return;

    m_feedChangedQueued = true;

    QTimer::singleShot(0, q, [this, q]() {
        Q_EMIT q->feedChanged();
        m_feedChangedQueued = false;
    });
}

FeedItemModel::FeedItemModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new FeedItemModelPrivate(this))
{
    QObject::connect(this, &QAbstractListModel::modelReset,
                     this, &FeedItemModel::countChanged);
    QObject::connect(this, &QAbstractListModel::rowsInserted,
                     this, &FeedItemModel::countChanged);
    QObject::connect(this, &QAbstractListModel::rowsRemoved,
                     this, &FeedItemModel::countChanged);
}

FeedItemModel::~FeedItemModel() = default;

void FeedItemModel::setUrl(const QUrl &url)
{
    Q_D(FeedItemModel);

    if (url == d->m_feed.url) return;
    d->m_feed = Feed();
    d->m_feed.url = url;
    Q_EMIT urlChanged();
    d->updateFeed();
}

QUrl FeedItemModel::url() const
{
    Q_D(const FeedItemModel);
    return d->m_feed.url;
}

bool FeedItemModel::isLoading() const
{
    Q_D(const FeedItemModel);
    return d->m_reply;
}

QVariantMap FeedItemModel::feedMap() const
{
    Q_D(const FeedItemModel);
    return d->m_feed.toMap();
}

void FeedItemModel::refresh()
{
    Q_D(FeedItemModel);
    return d->updateFeed();
}

QVariant FeedItemModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

QVariant FeedItemModel::data(const QModelIndex &index, int role) const
{
    Q_D(const FeedItemModel);

    Q_ASSERT(index.column() == 0);

    int row = index.row();
    if (Q_UNLIKELY(row < 0 || row >= rowCount())) {
        qWarning() << "Invalid index requested:" << row;
        return QVariant();
    }

    const FeedItem &item = d->m_items[row];
    switch (role) {
    case TitleRole:
        return item.title;
    case DescriptionRole:
        return item.description;
    case DescriptionFormatRole:
        return item.descriptionFormat;
    case DateRole:
        return item.date;
    case UrlRole:
        return item.url;
    case ThumbnailRole:
        return item.thumbnail;
    default:
        return QVariant();
    }
}

int FeedItemModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const FeedItemModel);
    Q_ASSERT(!parent.isValid());
    return d->m_items.count();
}

QHash<int, QByteArray> FeedItemModel::roleNames() const
{
    Q_D(const FeedItemModel);
    return d->m_roles;
}
