import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

GridLayout {
    id: root

    property var download: null

    columns: 4
    columnSpacing: 8

    AnimatedFlipable {
        Layout.fillWidth: true
        Layout.columnSpan: 3
        flipped: root.download.status == Download.DownloadComplete
        front: ProgressBar {
            anchors.fill: parent
            value: root.download.progress
            to: 100
            indeterminate: root.download.status == Download.NotStarted
        }
        back: Label {
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            textFormat: Text.RichText
            text: qsTr("<a href=\"%1\">Open file</a>").
                arg(Utils.localFileUrl(root.download.filePath))
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }

    AnimatedFlipable {
        id: flipable
        Layout.rowSpan: 2
        flipped: root.download.status > Download.Downloading
        front: Button {
            id: stopButton
            flat: true
            text: qsTr("\u{1f5d9} Cancel")
            onClicked: root.download.stop()
        }
        back: Image {
            sourceSize { width: height; height: stopButton.implicitHeight - 8 }
            source: root.download.status == Download.DownloadComplete ?
                "qrc:/icons/success" : "qrc:/icons/failed"
        }
    }

    Label {
        Layout.fillWidth: true
        elide: Text.ElideRight
        textFormat: Text.PlainText
        text: root.download.fileName
    }

    Label {
        textFormat: Text.PlainText
        text: qsTr("S: %1").arg(root.download.fileSize)
    }

    Label {
        Layout.leftMargin: 24
        textFormat: Text.PlainText
        text: root.download.downloadSpeed
        color: "green"
        visible: root.download.status == Download.Downloading
    }

    Label {
        Layout.fillWidth: true
        Layout.columnSpan: 4
        wrapMode: Text.WordWrap
        color: "red"
        text: root.download.errorString
        visible: text != ""
    }
}
