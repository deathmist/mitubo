import QtQml 2.2
import QtQuick 2.7
import QtQuick.Controls 2.12
import MiTubo 1.0
import "popupUtils.js" as PopupUtils

ApplicationWindow {
    id: root

    width: Settings.windowSize.width
    height: Settings.windowSize.height
    visible: true

    property bool youtubeDlRunning: false

    Component.onCompleted: {
        pageStack.forceActiveFocus()
        var args = Qt.application.arguments
        for (var i = 1; i < args.length; i++) {
            console.log("Got arg: " + args[i])
            if (args[i][0] != '-') {
                RequestDispatcher.requestOpenUrl(args[i]);
            }
        }
    }
    onActiveFocusControlChanged: console.log("Active focus control: " + activeFocusControl)
    onActiveFocusItemChanged: console.log("Active focus item: " + activeFocusItem)

    PlatformInitialization {}

    StackView {
        id: pageStack
        anchors.fill: parent
        initialItem: youtubeDl.installed ?
            mainPageComponent : downloaderIntroPage
    }

    MainPlaylistModel {
        id: playlistModel
        downloader: downloader
    }
    FeedModel { id: feedModelId }

    Component {
        id: mainPageComponent
        MainPage {
            id: mainPage
            feedModel: feedModelId
            playlists: playlistModel
            activeDownloadModel: activeDownloads
            onUrlAdded: requestOpenUrl(videoUrl)
            onSearchRequested: startSearch(searchText)
            onPlaylistPageRequested: {
                var page = pageStack.push(Qt.resolvedUrl("PlaylistsPage.qml"),
                                          { 'playlistModel': playlistModel })
                page.infoRequested.connect(function(videoData) {
                    root.showVideoInfo(videoData)
                })
                page.playRequested.connect(function (videoData) {
                    RequestDispatcher.requestOpenVideo(videoData)
                })
            }
            onInfoRequested: RequestDispatcher.requestOpenUrl(videoUrl, "info")
            onFeedAdded: {
                root.openFeedPage(feedModelId, -1, {
                    'feedUrl': feedUrl,
                    'subscribed': true,
                })
            }
            onFeedClicked: {
                root.openFeedPage(feedModel, index, {
                    'feedUrl': feedModel.get(index, "url"),
                    'subscribed': true,
                })
            }
        }
    }

    Component {
        id: downloaderIntroPage
        DownloaderIntroPage {
            onFinished: {
                youtubeDl.checkIsInstalled()
                pageStack.push(mainPageComponent)
            }
        }
    }

    BusyOverlay {
        id: busyOverlay
        visible: youtubeDlRunning
        message: qsTr("Retrieving stream information from the web page…")
        onClicked: youtubeDlRunning = false
    }

    VideoExtractor {
        id: downloader
        youtubeDl: YoutubeDl {
            id: youtubeDl
            applicationName: Qt.application.displayName
        }
    }

    ActiveDownloadModel {
        id: activeDownloads
        downloader: youtubeDl
    }

    Connections {
        target: RequestDispatcher
        onOpenUrlRequested: root.openUrl(url, action)
        onOpenVideoRequested: root.openVideo(videoData, action)
    }

    Connections {
        target: Qt.application
        onAboutToQuit: Settings.windowSize = Qt.size(root.width, root.height)
    }

    function showVideoInfo(json) {
        var page = pageStack.push(Qt.resolvedUrl("VideoInfoPage.qml"), {
            "videoData": json,
            "playlistModel": playlistModel,
        })
        page.playRequested.connect(function() {
            if (json.formats) {
                startPlayer(json)
            } else {
                openVideo(json, "play")
            }
        })
        page.downloadRequested.connect(function() {
            if (json.formats) {
                openDownloadPage(json)
            } else {
                openVideo(json, "download")
            }
        })
    }

    function startPlayer(json) {
        var page = pageStack.push(Qt.resolvedUrl("PlayerScreen.qml"), {
            "videoData": json,
            "playlistModel": playlistModel,
        })
        page.playUrlRequested.connect(function(url) {
            RequestDispatcher.requestOpenUrl(url)
        })
        page.downloadRequested.connect(function() {
            openDownloadPage(json)
        })
    }

    function onUrlOpened(inputData, json, action) {
        if (!youtubeDlRunning) return // operation was aborted
        youtubeDlRunning = false
        if (json["error"]) {
            console.log("Showing error screen with " + json["error"])
            var popup = PopupUtils.open(Qt.resolvedUrl("DownloaderErrorPopup.qml"), root, {
                "response": json,
            })
            return
        }
        var videoData = pickExtractedVideo(json)
        if (!videoData) {
            console.warn("onUrlOpened: empty video data")
            return
        }
        // merge the two objects
        for (var key in inputData) {
            videoData[key] = inputData[key]
        }
        if (videoData.isPlaylist) {
            var page = pageStack.push(Qt.resolvedUrl("PlaylistPage.qml"), {
                "playlist": playlistModel.playlistFromDownloader(json),
                "playlistModel": playlistModel,
            })
            page.infoRequested.connect(function(videoData) {
                root.showVideoInfo(videoData)
            })
            page.playRequested.connect(function (videoData) {
                RequestDispatcher.requestOpenVideo(videoData)
            })
        } else {
            if (action == "play") {
                startPlayer(videoData)
            } else if (action == "info") {
                showVideoInfo(videoData)
            } else if (action == "download") {
                openDownloadPage(videoData)
            }
        }
    }

    function pickExtractedVideo(json) {
        if ("items" in json && json["items"].length == 1) {
            return json["items"][0]
        } else {
            return json
        }
    }

    function openVideo(videoData, action) {
        youtubeDlRunning = true
        if (!action) action = "play"
        var url = videoData.inputUrl ? videoData.inputUrl : videoData.webpage_url
        downloader.getUrlInfo(url, function (json) {
            onUrlOpened(videoData, json, action)
        })
    }

    function openUrl(url, action) {
        if (pageStack.currentItem.objectName == "playerScreen") {
            console.log("Not handling request on player screen")
            return
        }
        youtubeDlRunning = true
        if (!action) action = "play"
        downloader.getUrlInfo(url, function (json) {
            onUrlOpened(null, json, action)
        })
    }

    function requestOpenUrl(videoUrl) {
        var item = playlistModel.itemForUrl(videoUrl)
        if (item) {
            RequestDispatcher.requestOpenVideo(item)
        } else {
            RequestDispatcher.requestOpenUrl(videoUrl)
        }
    }

    function startSearch(searchText) {
        var page = pageStack.push(Qt.resolvedUrl("SearchPage.qml"), {
            "searchText": searchText,
        })
        page.urlAdded.connect(function(videoUrl, action) {
            RequestDispatcher.requestOpenUrl(videoUrl, action)
        })
        page.openFeedRequested.connect(function(feedUrl) {
            root.openFeedPage(feedModelId, -1, {
                'feedUrl': feedUrl,
                'subscribed': false,
            })
        })
        page.openPlaylistRequested.connect(root.openUrl)
    }

    function openFeedPage(feedModel, feedIndex, params) {
        params['playlistModel'] = playlistModel
        var page = pageStack.push(Qt.resolvedUrl("FeedPage.qml"), params)
        page.playRequested.connect(root.requestOpenUrl)
        page.infoRequested.connect(function(videoUrl) {
            RequestDispatcher.requestOpenUrl(videoUrl, "info")
        })
        page.finished.connect(function () {
            if (page.subscribed) {
                feedModel.addOrUpdateFeed(page.feed)
            } else if (feedIndex >= 0) {
                feedModel.deleteFeed(feedIndex)
            }
        })
        return page
    }

    function openDownloadPage(videoData) {
        console.log("Opening download page")
        var page = pageStack.push(Qt.resolvedUrl("DownloadPage.qml"), {
            "videoData": videoData,
        })
        page.downloadRequested.connect(function(videoData, mediaFormat) {
            console.log("Downloading video format " + JSON.stringify(mediaFormat))
            var download = youtubeDl.download(videoData.inputUrl, mediaFormat)
            page.download = download
            activeDownloads.addDownload(videoData, download)
        })
    }

}
