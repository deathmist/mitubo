import QtQuick 2.7
import "Constants.js" as UI

MouseArea {
    id: root

    property real value: 1.0
    readonly property bool interacting: drag.active

    signal volumeRequested(real volume)

    height: UI.OsdButtonHeight
    width: height * 2

    onPressed: dragTarget.x = mouseX * 2 - width / 2
    drag.target: dragTarget
    drag.axis: Drag.XAxis
    drag.minimumX: -width / 2
    drag.maximumX: width * 1.5

    Item {
        id: mask
        anchors { left: parent.left; top: parent.top; bottom: parent.bottom }
        width: parent.width * root.value
        clip: true

        Image {
            width: image.width
            height: image.height
            source: "qrc:/icons/volume-on"
            sourceSize { width: width; height: height }
            smooth: true
        }
    }

    Image {
        id: image
        anchors.fill: parent
        source: "qrc:/icons/volume-off"
        sourceSize { width: width; height: height }
        smooth: true
    }

    Item {
        id: dragTarget
        width: 1
        height: 1
        onXChanged: {
            var volume = (x + root.width / 2) / (root.width * 2)
            root.volumeRequested(volume)
        }
    }
}
