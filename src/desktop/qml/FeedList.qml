import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12

DraggableListView {
    id: root

    property var playlistModel: null

    signal playRequested(url videoUrl)
    signal feedClicked(var feedModel, int feedIndex)
    signal folderClicked(int index)

    signal refreshRequested()

    property bool _draggingFolder: draggedItem &&
                                   draggedItem.contentItem.isFolder

    implicitHeight: contentHeight
    ScrollBar.vertical: ScrollBar {}
    clip: !dragging
    spacing: 8
    delegate: DraggableDelegate {
        id: dragDelegate
        width: root.width
        index: model.index
        acceptDrops: model.isFolder && !root._draggingFolder
        objectName: model.title

        FeedDelegate {
            id: feedDelegate
            width: root.width
            feedUrl: model.url
            title: model.title
            description: model.description || ""
            isFolder: model.isFolder
            logoUrl: model.logoUrl
            playlistModel: root.playlistModel
            onMoveRequested: root.moving = true
            onUnsubscriptionRequested: root.model.deleteFeed(index)
            onClicked: isFolder ?
                root.folderClicked(index) : root.feedClicked(root.model, index)
            onPlayRequested: root.playRequested(videoUrl)
            onFeedClicked: root.feedClicked(feedModel, feedIndex)

            Connections {
                target: root
                onRefreshRequested: feedDelegate.refresh()
            }

            DropIndicator {
                anchors.fill: parent
                opacity: dragDelegate.coveredForDrop ? 0.8 : 0
            }
        }
    }
    removeDisplaced: Transition {
         NumberAnimation { properties: "y"; duration: 400; easing.type: Easing.OutBounce }
    }

    onMoveRequested: model.moveFeed(indexFrom, indexTo)
    onMoveIntoRequested: model.moveFeedInto(indexFrom, indexInto)

    function refresh() {
        refreshRequested()
    }
}
