import QtQml 2.9

QtObject {
    id: root

    property string searchText: ""
    property var engine: null
    property int selectedProvider: 0

    property var model: [
        {
            name: "Yandex",
            icon: "qrc:/icons/search-yandex",
            component: yandexSearch,
        },
        {
            name: "PeerTube",
            icon: "qrc:/icons/search-peertube",
            component: peerTubeSearch,
        },
        {
            name: "YouTube",
            icon: "qrc:/icons/search-youtube",
            component: youtubeSearch,
        }
    ]

    property var yandexSearch: Component {
        YandexSearch {}
    }

    property var youtubeSearch: Component {
        InvidiousSearch {}
    }

    property var peerTubeSearch: Component {
        PeerTubeSearch {}
    }

    Component.onCompleted: activateProvider(selectedProvider)
    onSelectedProviderChanged: activateProvider(selectedProvider)

    function activateProvider(index) {
        var component = model[index].component
        engine = component.createObject(root, {
            "searchText": Qt.binding(function() { return root.searchText }),
        })
    }
}
