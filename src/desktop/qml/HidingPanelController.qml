import QtQuick 2.7

MouseArea {
    id: root

    property bool forceOpen: false
    readonly property string effectiveState: state == "locked" ? "exposed" : state

    anchors.fill: parent
    hoverEnabled: true
    z: 1
    onPressed: { show(); mouse.accepted = false }
    onPositionChanged: panelTimer.restart()

    states: [
        State { id: exposedState; name: "exposed" },
        State {
            name: "locked"
            extend: "exposed"
            when: root.forceOpen
            PropertyChanges { target: panelTimer; running: false }
            PropertyChanges { target: root; enabled: false }
        }
    ]

    Timer {
        id: panelTimer
        interval: 2000
        onTriggered: root.hide()
    }

    function show() {
        if (state == "locked") return
        state = "exposed"
        console.log("controller showing")
        panelTimer.restart()
    }

    function hide() {
        state = ""
    }

    function lock() {
        state = "locked"
    }

    function unlock() {
        state = (state == "locked") ? "exposed" : ""
    }
}
