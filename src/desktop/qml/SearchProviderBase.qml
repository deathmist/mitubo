import QtQml 2.2
import QtQml.Models 2.2

QtObject {
    id: root

    property string searchText: ""
    property var searchOptions: ({})
    property string searchType: "videos"
    property var model: ListModel {}
    property var optionsComponent: null

    onSearchTextChanged: __timer.restart()
    onSearchOptionsChanged: __timer.restart()
    onSearchTypeChanged: model = listModelComponent.createObject(root)

    property var _listModelComponent: Component {
        id: listModelComponent
        ListModel {}
    }

    property var __timer: Timer {
        interval: 50
        onTriggered: runSearch()
    }

    function runSearch() {
        // To be reimplemented
    }

    function buildParameters() {
        var parameters = ""
        for (var key in searchOptions) {
            var value = searchOptions[key]
            if (value) {
                parameters += key + "=" + encodeURIComponent(value) + "&"
            }
        }
        return parameters
    }
}
