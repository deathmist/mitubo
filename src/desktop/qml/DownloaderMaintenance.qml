import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Drawer {
    id: root

    signal finished()

    edge: Qt.BottomEdge
    interactive: false
    modal: false
    width: parent.width
    onClosed: if (!updating) finished()

    property bool updating: false

    RowLayout {
        anchors { left: parent.left; right: parent.right; bottom: parent.bottom }

        Label {
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: qsTr("A new version (%1) of <b>%2</b> is available; <a href=\"foo\">click here</a> to install it.")
                .arg(installer.latestVersion)
                .arg(installer.programName)
            onLinkActivated: root.update()
        }

        Button {
            id: doneButton
            text: qsTr("Hide")
            onClicked: {
                root.close()
            }
        }
    }

    YoutubeDlInstaller {
        id: installer
        onStatusChanged: {
            console.log("Installer status: " + status)
            if (status == YoutubeDlInstaller.UpdateAvailable) {
                root.open()
            }
        }
        autoDownload: false
        Component.onCompleted: checkForUpdates()
    }

    function update() {
        root.close()
        root.updating = true
        var page =
            pageStack.push(Qt.resolvedUrl("DownloaderInstallationPage.qml"), {
        })
        page.finished.connect(function() {
            root.finished()
        })
    }
}
