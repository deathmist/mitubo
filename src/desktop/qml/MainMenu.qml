import QtQuick 2.7
import QtQuick.Controls 2.12
import "popupUtils.js" as PopupUtils

Menu {
    id: root

    property var activeDownloadModel: null
    property var pageStack: ApplicationWindow.contentItem ?
        ApplicationWindow.contentItem.children[0] : null

    MenuItem {
        // TODO: use Actions with Qt > 5.10
        text: qsTr("Downloads…")
        onTriggered: pageStack.push(Qt.resolvedUrl("DownloadsPage.qml"), {
            'activeDownloadModel': root.activeDownloadModel,
        })
    }

    MenuSeparator {}

    MenuItem {
        text: qsTr("Check for updates…")
        onTriggered: PopupUtils.open(Qt.resolvedUrl("UpdateDialog.qml"),
                                     pageStack.currentItem, {})
    }

    MenuItem {
        text: qsTr("About MiTubo…")
        onTriggered: PopupUtils.open(Qt.resolvedUrl("AboutPopup.qml"),
                                     pageStack.currentItem, {})
    }
}
