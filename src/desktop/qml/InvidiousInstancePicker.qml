import QtQml 2.2

QtObject {
    id: root

    property string chosenInstance: ""

    property var _instances: [
        "invidious.snopyta.org",
        // "yewtu.be", always responds "Too many requests"
        "vid.puffyan.us",
        "invidious.namazso.eu",
        "inv.riverside.rocks",
        "invidious.osi.kr",
        "youtube.076.ne.jp",
        "yt.artemislena.eu",
        "invidious.flokinet.to",
        "invidious.weblibre.org",
        "invidious.esmailelbob.xyz",
    ]
    property int _activeRequests: 0
    property int _nextInstance: 0
    property int _bestResponseTime: 1000000 // just a high number
    property int _bestInstance: 0
    property int _goodResponseTime: 300
    property int _maxActiveRequests: 4
    property bool _done: false

    /* The callback is invoked with the instance version and the number of
     * milliseconds that it took to reply */
    function testInstance(instance, callback) {
        var startTime = new Date()
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                try {
                    var stats = JSON.parse(xhr.responseText)
                } catch (e) {
                    console.warn("Could not parse JSON from instance " + instance)
                    callback("", -1)
                    return
                }
                var endTime = new Date()
                var elapsed = endTime - startTime
                callback(stats.version, elapsed)
            }
        }

        var url = "https://" + instance + "/api/v1/stats"
        xhr.open("GET", url)
        xhr.send()
    }

    /* Tests the next instance. Returns false if there are no more instances to
     * test; returns true if the method must be invoked again once a callback
     * is triggered.
     * The callback is invoked with true if testNextInstance() has to be called
     * again or with false if the search can be terminated early (because we
     * found an instance with an acceptable ping time already). */
    function testNextInstance(callback) {
        _activeRequests++

        var currentInstance = _nextInstance++
        var instance = _instances[currentInstance]
        testInstance(instance, function(version, responseTime) {
            _activeRequests--
            console.log("Instance " + instance + " version " + version + " replied in " + responseTime)
            if (responseTime > 0 && responseTime < _bestResponseTime) {
                _bestResponseTime = responseTime
                _bestInstance = currentInstance
                if (responseTime < _goodResponseTime) {
                    callback(false)
                    return
                }
            }
            callback(true)
        })
    }

    function testInstances() {
        while (!_done &&
               _activeRequests < _maxActiveRequests &&
               _nextInstance < _instances.length) {
            testNextInstance(function(mustContinue) {
                if (mustContinue) {
                    testInstances()
                } else {
                    _done = true
                }
            })
        }

        if (_done || _activeRequests == 0) {
            console.log("Pick completed!")
            root.chosenInstance = _instances[_bestInstance]
        }
    }

    function pickInstance() {
        testInstances()
    }
}
