import QtQml 2.2
import MiTubo 1.0

PlaylistModel {
    id: root

    property var downloader: null

    property int _updateCounter: 0
    property var _c1: Connections {
        target: playlistById(continueWatchingListId)
        onItemsChanged: _updateCounter++
    }
    property var _c2: Connections {
        target: playlistById(watchHistoryListId)
        onItemsChanged: _updateCounter++
    }

    function addToContinueWatching(videoData, position) {
        // Remove the item from the "watch later" videos
        var list = playlistById(watchLaterListId)
        if (list) {
            var index = list.indexOf(videoData)
            if (index >= 0) list.takeAt(index)
        }

        // Add video to the "continue watching" list
        var list = playlistById(continueWatchingListId)
        if (!list) return

        list.addVideo(videoData, position)
    }

    function addWatchedVideo(videoData) {
        // Remove the item from the partially watched videos
        var list = playlistById(continueWatchingListId)
        if (list) {
            var index = list.indexOf(videoData)
            if (index >= 0) list.takeAt(index)
        }

        // Add video to the watch history
        list = playlistById(watchHistoryListId)
        if (list) {
            list.addVideo(videoData)
        }
    }

    function addToWatchLater(videos, onTop) {
        var list = playlistById(watchLaterListId)
        if (list) {
            var index = onTop ? 0 : list.count
            list.addVideos(videos, index)
            for (var i = 0; i < videos.length; i++) {
                var video = videos[i]
                // If the video consists only of the URL, fetch video info
                if (typeof video == "string") {
                    updateVideoInfo(list, video)
                }
            }
        }
    }

    function itemForUrl(url) {
        var list = playlistById(continueWatchingListId)
        var i = list.indexOf(url)
        if (i < 0) return null
        return list.itemAt(i)
    }

    function videoWasWatched(url) {
        var list = playlistById(watchHistoryListId)
        return list.indexOf(url) >= 0
    }

    function watchedProgress(url) {
        if (_updateCounter, videoWasWatched(url)) {
            return 1.0
        } else {
            var list = playlistById(continueWatchingListId)
            var i = list.indexOf(url)
            if (i < 0) return 0.0
            var item = list.itemAt(i)
            var start = item.startTime / 1000.0
            return item.duration > 0 ?
                Math.min(start / item.duration, 1.0) : 0.5
        }
    }

    function updateVideoInfo(list, url) {
        console.log("updateVideoInfo called for " + url)
        downloader.getUrlInfo(url, function(json) {
            json = pickExtractedVideo(json)
            if (json) {
                list.updateVideo(json)
            }
        })
    }

    function hasWatchLater() {
        var list = playlistById(watchLaterListId)
        return list && list.count > 0
    }

    function firstInWatchLater() {
        var list = playlistById(watchLaterListId)
        if (list && list.count) {
            return list.itemAt(0)
        } else {
            console.warn("Requested next video, but playlist empty")
            return {}
        }
    }
}
