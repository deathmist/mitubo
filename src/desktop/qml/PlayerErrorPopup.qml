import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Dialog {
    id: root

    property int errorCode: 0
    property string errorMessage

    property bool hasMoreStreams: false

    title: qsTr("Playback error")
    standardButtons: Dialog.Close
    modal: true
    anchors.centerIn: parent

    ColumnLayout {
        anchors.fill: parent
        spacing: 12

        Label {
            Layout.fillWidth: true
            Layout.maximumWidth: 500
            wrapMode: Text.WordWrap
            text: qsTr("An error occurred during media playback (%1): %2").
                arg(root.errorCode).arg(root.errorMessage)
        }

        Label {
            Layout.fillWidth: true
            Layout.maximumWidth: 500
            wrapMode: Text.WordWrap
            visible: root.hasMoreStreams
            text: qsTr("You can also try to switch to a different media format by clicking on the \u2699 symbol near the bottom right corner.")
        }
    }
}
