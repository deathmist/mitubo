import QtQuick 2.9
import QtQuick.Controls 2.12

Rectangle {
    id: root

    color: "white"
    opacity: 0.8

    Behavior on opacity {
        NumberAnimation { duration: 300 }
    }

    Label {
        anchors.fill: parent
        text: qsTr("+")
        font.pixelSize: height
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}
