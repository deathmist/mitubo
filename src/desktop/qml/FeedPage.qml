import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Page {
    id: root

    property alias feedUrl: feedItemModel.url
    property bool subscribed: false
    property alias feed: feedItemModel.feed
    property var playlistModel: null

    signal playRequested(var videoUrl)
    signal infoRequested(url videoUrl)
    signal finished()

    title: qsTr("RSS feed")

    header: TitleHeader {
        onClosed: root.finished

        ToolButton {
            text: root.subscribed ? "★" : "☆"
            font.pointSize: 16
            ToolTip.text: root.subscribed ? qsTr("Subscribed") : qsTr("Subscribe")
            onClicked: root.subscribed = !root.subscribed
            ToolTip {
                id: subscribePopup
                text: qsTr("Click to subscribe \u2191")
                margins: -1
                timeout: 3000
                x: parent.x + parent.width - parent.padding - width
                enter: Transition {
                    PropertyAnimation {
                        property: "opacity"
                        from: 0.0
                        to: 1.0
                        duration: 500
                        easing.type: Easing.Linear
                    }
                    SequentialAnimation {
                        NumberAnimation {
                            property: "y"
                            from: subscribePopup.parent.height
                            to: from + 30
                            duration: 400
                            easing.type: Easing.InOutSin
                        }
                        NumberAnimation {
                            property: "y"
                            to: subscribePopup.parent.height
                            from: to + 30
                            duration: 400
                            easing.type: Easing.InOutSin
                        }
                        loops: 2
                    }
                }
                exit: Transition {
                    PropertyAnimation {
                        property: "opacity"
                        to: 0.0
                        duration: 1000
                        easing.type: Easing.Linear
                    }
                }
            }
            Component.onCompleted: if (!root.subscribed) subscribePopup.open()
        }
    }

    Component {
        id: headerComponent
        Pane {
            anchors { left: parent.left; right: parent.right; margins: 12 }
            z: 2

            RowLayout {
                anchors.fill: parent

                ColumnLayout {
                    Layout.fillWidth: true

                    Label {
                        Layout.fillWidth: true
                        text: feedItemModel.feed.title
                        font.pointSize: root.font.pointSize * 2
                        elide: Text.ElideRight
                    }

                    Label {
                        Layout.fillWidth: true
                        text: feedItemModel.feed.description
                        wrapMode: Text.WordWrap
                    }
                }

                Image {
                    source: feedItemModel.feed.logoUrl
                    sourceSize { width: 64; height: 64 }
                    fillMode: Image.PreserveAspectFit
                }
            }
        }
    }

    ListView {
        id: listView
        ScrollBar.vertical: ScrollBar {}
        anchors.fill: parent
        header: headerComponent
        headerPositioning: ListView.PullBackHeader
        model: feedItemModel
        delegate: PlaylistItemDelegate {
            x: 12
            width: listView.width - 24
            canDelete: false
            showMoreInfoButton: true
            title: model.title
            description: model.description || ""
            descriptionFormat: model.descriptionFormat
            previewUrl: model.thumbnail
            watchProgress: root.playlistModel.watchedProgress(model.url)
            pageUrl: model.url
            onPlayRequested: root.playRequested(model.url)
            onInfoRequested: root.infoRequested(model.url)
        }
    }

    FeedItemModel {
        id: feedItemModel
    }
}
