import QtQuick 2.7

QtObject {
    id: root

    property var formats: []
    property int currentFormatIndex: -1

    property var streamModel: []
    /* This array contains, at each index `i`, the list of formats for the
     * stream configuration at streamModel[i] */
    property var formatsPerStream: []

    onFormatsChanged: prepareModels()

    function selectActiveFormat() {
        for (var i = 0; i < formatsPerStream.length; i++) {
            var formats = formatsPerStream[i]
            for (var j = 0; j < formats.length; j++) {
                var format = formats[j]
                if (format["index"] == currentFormatIndex) {
                    streamSelector.currentIndex = i
                    formatSelector.currentIndex = j
                    return
                }
            }
        }
    }

    function prepareModels() {
        var audioVideoFormats = []
        var audioOnlyFormats = []
        var videoOnlyFormats = []
        for (var i = 0; i < formats.length; i++) {
            var format = formats[i]
            format["label"] = qsTr("%1 (bitrate: %2)").arg(format.format_note).arg(format.tbr)
            format["index"] = i
            if (format.acodec != "none" && format.vcodec != "none") {
                audioVideoFormats.push(format)
            } else if (format.acodec != "none") {
                audioOnlyFormats.push(format)
            } else {
                videoOnlyFormats.push(format)
            }
        }
        var streamModel = []
        var formatsPerStream = []
        if (audioVideoFormats.length > 0) {
            streamModel.push(qsTr("Audio + video"))
            formatsPerStream.push(audioVideoFormats)
        }
        if (audioOnlyFormats.length > 0) {
            streamModel.push(qsTr("Audio only"))
            formatsPerStream.push(audioOnlyFormats)
        }
        if (videoOnlyFormats.length > 0) {
            streamModel.push(qsTr("Video only"))
            formatsPerStream.push(videoOnlyFormats)
        }
        if (streamModel.length == 0) {
            console.warn("No audio or video formats found")
        }
        root.streamModel = streamModel
        console.log("Stream model: " + JSON.stringify(streamModel))
        root.formatsPerStream = formatsPerStream
        selectActiveFormat()
    }
}
