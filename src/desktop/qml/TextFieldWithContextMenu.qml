import QtQuick 2.9
import QtQuick.Controls 2.12

TextField {
    id: root

    persistentSelection: true
    onReleased: if (event.button == Qt.RightButton) showContextMenu()
    onPressAndHold: showContextMenu()

    Menu {
        id: menu
        y: root.height

        MenuItem {
            text: qsTr("Cut")
            enabled: root.selectedText != ""
            onClicked: { root.cut(); root.deselect(); }
        }

        MenuItem {
            text: qsTr("Copy")
            enabled: root.selectedText != ""
            onClicked: root.copy()
        }

        MenuItem {
            text: qsTr("Paste")
            enabled: root.canPaste
            onClicked: { root.paste(); root.deselect(); }
        }

        MenuItem {
            text: qsTr("Delete")
            enabled: root.selectedText != ""
            onClicked: { root.remove(root.selectionStart, root.selectionEnd); root.deselect(); }
        }

        MenuSeparator {}

        MenuItem {
            text: qsTr("Select all")
            enabled: root.length > 0
            onClicked: root.selectAll()
        }
    }

    function showContextMenu() {
        menu.open()
    }
}
