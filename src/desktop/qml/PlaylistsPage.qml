import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Page {
    id: root

    property var playlistModel: null

    signal playRequested(var videoData)
    signal infoRequested(var videoData)
    signal downloadRequested()

    title: qsTr("Playlists")

    header: TitleHeader {}

    ListView {
        ScrollBar.vertical: ScrollBar {}
        anchors.fill: parent
        model: root.playlistModel
        delegate: PlaylistDelegate {
            x: 12
            width: ListView.view.width - 24
            text: model.name
            subText: qsTr("%n elements", "", model.count)
            onClicked: root.openPlaylist(model.playlist)
            onDeletionRequested: playlistModel.deletePlaylist(index)
        }
        footer: PlaylistItemNew {
            x: 4
            width: parent.width - 8
            onPlaylistNameChosen: playlistModel.createPlaylist(playlistName, [])
        }
    }

    function openPlaylist(playlist) {
        var pageStack = StackView.view
        var page = pageStack.push(Qt.resolvedUrl("PlaylistPage.qml"), {
            "playlist": playlist,
            "playlistModel": root.playlistModel,
        })
        page.infoRequested.connect(root.infoRequested)
        page.playRequested.connect(root.playRequested)
    }
}
