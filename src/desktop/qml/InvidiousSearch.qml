import QtQml 2.2
import QtQml.Models 2.2

SearchProviderBase {
    id: root

    optionsComponent: Component {
        InvidiousSearchOptions {}
    }
    searchOptions: {
        "type": "video",
        "sort_by": "relevance",
    }
    searchType: {
        switch (searchOptions.type) {
        case "channel": return "channels"
        case "playlist": return "playlists"
        default: return "videos"
        }
    }

    property string _searchUrl: ""
    property string _instance: instancePicker.chosenInstance
    property int _currentPage: -1

    property var _picker: InvidiousInstancePicker {
        id: instancePicker
        Component.onCompleted: pickInstance()
    }

    function fetchPage() {
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                parseResponse(xhr.responseText)
            }
        }

        var url = _searchUrl
        _currentPage += 1
        if (_currentPage > 0) {
            url += '&page=' + _currentPage
        }
        xhr.open("GET", url)
        xhr.send()
    }

    function fieldsForType(type) {
        switch (searchType) {
        case "channels":
            return "author,description,authorUrl,authorId,authorThumbnails"
        case "playlists":
            return "title,playlistId,author,videoCount"
        default:
            return "author,authorId,title,description,videoId,lengthSeconds,videoThumbnails,published"
        }
    }

    function runSearchReady() {
        model.clear()
        var parameters = buildParameters()
        var fields = fieldsForType(searchType)
        _searchUrl = "https://" + _instance + "/api/v1/search?" +
            "fields=" + fields + "&" +
            parameters +
            "q=" + encodeURIComponent(root.searchText)
        _currentPage = -1
        fetchPage()
    }

    function runSearch() {
        if (instancePicker.chosenInstance) {
            runSearchReady()
        } else {
            instancePicker.chosenInstanceChanged.connect(function() {
                console.log("Chosen instance changed to " + _instance)
                runSearchReady()
            })
        }
    }

    function fetchMore() {
        fetchPage();
    }

    function unquoteHtml(text) {
        return text
            .replace(/&quot;/g, '"')
            .replace(/&apos;/g, "'")
            .replace(/&amp;/g, '&');
    }

    function thumbnailUrl(thumbnails, minSize) {
        var l = thumbnails.length
        var chosen = null
        for (var i = 0; i < l; i++) {
            var t = thumbnails[i]
            if (!chosen || (t.width >= minSize && t.width < chosen.width)) {
                chosen = t;
            }
        }
        if (!chosen) return ""
        var url = chosen.url
        if (url.startsWith("//")) {
            url = "https:" + url
        }
        return url
    }

    function parseResponse(json) {
        try {
            var items = JSON.parse(json)
        } catch (e) {
            console.warn("Could not parse JSON")
            return
        }
        var len = items.length
        for (var i = 0; i < len; i++) {
            var o = items[i]
            console.log("   Item: " + JSON.stringify(o))

            switch (root.searchType) {
            case "videos": parseVideo(o); break
            case "channels": parseChannel(o); break
            case "playlists": parsePlaylist(o); break
            }
        }
    }

    function parseVideo(o) {
        var now = new Date()
        var month = 30 * 24 * 60 * 60 * 1000 // 30 days
        var previewUrl = thumbnailUrl(o.videoThumbnails, 64)
        var datePublishedText = ""
        if (o.published > 0) {
            var datePublished = new Date(0)
            datePublished.setUTCSeconds(o.published)
            var monthsAgo = (now - datePublished) / month
            if (monthsAgo < 3) {
                datePublishedText = Qt.formatDate(datePublished)
            } else {
                if (monthsAgo > 12) {
                    datePublishedText = qsTr("%n year(s) ago", "", monthsAgo / 12)
                } else {
                    datePublishedText = qsTr("%n month(s) ago", "", monthsAgo)
                }
            }
        }

        var feedUrl = "https://www.youtube.com/feeds/videos.xml?channel_id=" + o.authorId

        // Use the same fields/roles for videos and channels, or the ListModel
        // will get confused (without using dynamicRoles)
        model.append({
            "url": "https://www.youtube.com/watch?v=" + o.videoId,
            "author": o.author,
            "feedUrl": feedUrl,
            "duration": o.lengthSeconds,
            "host": "",
            "title": o.title,
            "previewUrl": previewUrl,
            "description": o.description,
            "datePublished": datePublishedText,
        });
    }

    function parseChannel(o) {
        var previewUrl = thumbnailUrl(o.authorThumbnails, 64)
        var datePublishedText = ""

        var feedUrl = "https://www.youtube.com/feeds/videos.xml?channel_id=" + o.authorId
        model.append({
            "url": "https://www.youtube.com" + o.authorUrl,
            "feedUrl": feedUrl,
            "title": o.author,
            "previewUrl": previewUrl,
            "description": o.description,
        });
    }

    function parsePlaylist(o) {
        var playlistUrl =
            "https://www.youtube.com/playlist?list=" + o.playlistId
        model.append({
            "url": playlistUrl,
            "title": o.title,
            "author": o.author,
            "itemCount": o.videoCount,
        });
    }
}
