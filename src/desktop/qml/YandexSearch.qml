import QtQml 2.2
import QtQml.Models 2.2

SearchProviderBase {
    id: root

    optionsComponent: Component {
        YandexSearchOptions {}
    }

    property string _searchUrl: ""
    property int _currentPage: -1

    function fetchPage() {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                parseResponse(xhr.responseText);
            }
        }

        var url = _searchUrl;
        _currentPage += 1;
        if (_currentPage > 0) {
            url += '&p=' + _currentPage
        }
        xhr.open("GET", url);
        xhr.send();
    }

    function runSearch() {
        model.clear();
        var parameters = buildParameters()
        _searchUrl = "https://yandex.ru/video/search?" +
            parameters +
            "text=" +
            encodeURIComponent(root.searchText);
        _currentPage = -1;
        fetchPage();
    }

    function fetchMore() {
        fetchPage();
    }

    function unquoteHtml(text) {
        return text
            .replace(/&quot;/g, '"')
            .replace(/&apos;/g, "'")
            .replace(/&amp;/g, '&');
    }

    function parseResponse(html) {
        // TODO: use a HTML parser (gumbo, maybe?)
        var dataVideoString = 'data-video="';
        var backgroundImageString = 'background-image: url(';
        var titleString = 'serp-item__text';
        var nextPos = 0;
        for (var pos = html.indexOf(dataVideoString); pos >= 0; pos = nextPos) {
            nextPos = html.indexOf(dataVideoString, pos + 1);
            var block = html.slice(pos, nextPos);
            var dataBegin = 12;
            var dataEnd = block.indexOf('"', dataBegin);
            var json = unquoteHtml(block.slice(dataBegin, dataEnd))
            try {
                var o = JSON.parse(json)
            } catch (e) {
                console.warn("Could not parse JSON");
                continue;
            }

            var previewUrl = "";
            var i = block.indexOf('<img ', dataEnd);
            var previewUrlStart = -1;
            var previewUrlEnd = -1;
            if (i > 0) i = block.indexOf('src="', i);
            if (i > 0) {
                previewUrlStart = i + 5;
                previewUrlEnd = block.indexOf('"', previewUrlStart);
            }
            if (previewUrlEnd < 0) {
                // Try with the background style
                i = block.indexOf(backgroundImageString, dataEnd)
                if (i > 0) {
                    previewUrlStart = i + 22;
                    previewUrlEnd = block.indexOf(')', previewUrlStart);
                }
            }
            if (previewUrlEnd > 0) {
                previewUrl = block.slice(previewUrlStart, previewUrlEnd);
                if (previewUrl[0] == '/')
                    previewUrl = 'https:' + previewUrl;
            }

            // description
            var description = ""
            var descriptionStart = -1;
            var descriptionEnd = -1;
            i = block.indexOf(titleString, dataEnd);
            if (i > 0) i = block.indexOf('>', i);
            if (i > 0) {
                descriptionStart = i + 1;
                descriptionEnd = block.indexOf('</div>', descriptionStart);
            }
            if (descriptionEnd > 0) {
                description = block.slice(descriptionStart, descriptionEnd);
            }

            var duration = 0;
            try {
                duration = o.counters.toHostingLoaded.stredParams.duration;
            } catch (e) {}
            model.append({
                "url": o.url,
                "author": "",
                "feedUrl": "",
                "duration": duration,
                "title": unquoteHtml(o.title),
                "host": o.greenHost,
                "previewUrl": previewUrl,
                "description": unquoteHtml(description),
                "datePublished": "",
            });
        }
    }
}
