import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Popup {
    id: root

    property var searchOptions: ({})

    margins: 12

    onOpened: load()
    onClosed: save()

    property var __controls: ({
        "within": dateControl,
        "duration": lengthControl,
    })

    GridLayout {
        anchors.fill: parent
        columns: 2

        Label {
            Layout.fillWidth: true
            text: qsTr("Published:")
        }

        ComboBox {
            id: dateControl
            Layout.fillWidth: true
            Layout.minimumWidth: 200
            model: ListModel {
                ListElement { text: qsTr("Any time"); value: "" }
                ListElement { text: qsTr("Today"); value: "77" }
                ListElement { text: qsTr("Less than a week ago"); value: "9" }
                ListElement { text: qsTr("Less than a month ago"); value: "2" }
                ListElement { text: qsTr("Less than one year ago"); value: "5" }
            }
            textRole: "text"
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Duration:")
        }

        ComboBox {
            id: lengthControl
            Layout.fillWidth: true
            model: ListModel {
                ListElement { text: qsTr("Any"); value: "" }
                ListElement { text: qsTr("Less than 10 minutes"); value: "short" }
                ListElement { text: qsTr("10-65 minutes"); value: "medium" }
                ListElement { text: qsTr("More than 65 minutes"); value: "long" }
            }
            textRole: "text"
        }
    }

    function load() {
        for (var key in searchOptions) {
            if (!__controls.hasOwnProperty(key)) continue
            var control = __controls[key]

            // lookup the index
            var value = searchOptions[key]
            var model = control.model
            var index = 0 // Let the first one be the default
            for (var i = 0; i < model.count; i++) {
                if (model.get(i).value == value) {
                    index = i
                    break
                }
            }
            control.currentIndex = index
        }
    }

    function save() {
        var options = {}
        for (var key in __controls) {
            var control = __controls[key]
            var value = control.model.get(control.currentIndex).value
            if (value) {
                options[key] = value
            }
        }
        searchOptions = options
    }
}
