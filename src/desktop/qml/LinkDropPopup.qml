import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Popup {
    id: root

    property var urls: []

    signal watchLaterRequested(var urls, bool onTop)
    signal watchNowRequested(var url)

    margins: 8

    ColumnLayout {
        anchors.fill: parent

        Label {
            Layout.fillWidth: true
            text: qsTr("Link action:")
            font.bold: true
        }

        Frame {
            Layout.fillWidth: true
            ColumnLayout {
                Layout.fillWidth: true

                Repeater {
                    model: root.urls
                    Label {
                        Layout.fillWidth: true
                        text: "- <a href=\"%1\">%1</a>".arg(modelData)
                    }
                }
            }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Play now")
            visible: root.urls.length == 1
            onClicked: { root.watchNowRequested(root.urls[0]); root.close() }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Add to “Watch later” (first)")
            onClicked: { root.watchLaterRequested(root.urls, true); root.close() }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Add to “Watch later” (last)")
            onClicked: { root.watchLaterRequested(root.urls, false); root.close() }
        }
    }
}
