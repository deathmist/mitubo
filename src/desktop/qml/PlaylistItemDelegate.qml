import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

SwipeDelegate {
    id: root

    property string author
    property url feedUrl
    property alias title: titleLabel.text
    property alias description: descriptionLabel.text
    property alias descriptionFormat: descriptionLabel.textFormat
    property int duration: 0
    property alias host: hostLabel.text
    property alias previewUrl: preview.source
    property url pageUrl
    property bool expanded: false
    property bool showMoreInfoButton: false
    property bool canDelete: true
    property real watchProgress: 0.0
    property alias datePublished: dateLabel.text

    signal playRequested()
    signal infoRequested()
    signal openFeedRequested(url feedUrl)
    signal deletionRequested()

    swipe.left: RowLayout {
        anchors {
            top: parent.top; bottom: parent.bottom
            right: parent.background.left
        }
        DeleteButton {
            Layout.fillHeight: true
            visible: root.canDelete
            onClicked: root.deletionRequested()
        }
    }
    clip: true
    contentItem: RowLayout {
        id: layout
        property bool narrow: root.width < 400

        GridLayout {
            Layout.fillWidth: true
                    Layout.minimumWidth: 10
            Layout.maximumHeight: root.expanded ? -1 : layout.narrow ? (preview.height + titleLabel.height + bottomLayout.height + rowSpacing * 2) : 120
            columns: 2
            columnSpacing: 2
            rowSpacing: 2

            Image {
                id: preview
                Layout.rowSpan: layout.narrow ? 1 : 3
                Layout.row: layout.narrow ? 1 : 0
                Layout.column: 0
                Layout.minimumWidth: status == Image.Null ? 0 : sourceSize.width
                Layout.maximumWidth: sourceSize.width
                sourceSize.width: layout.narrow ? 80 : 120
                fillMode: Image.PreserveAspectFit

                VideoPreviewProgressBar {
                    id: progressBar
                    anchors {
                        left: parent.left; right: parent.right;
                        bottom: parent.bottom; margins: 2
                    }
                    value: root.watchProgress
                }
            }

            Label {
                id: titleLabel
                Layout.columnSpan: layout.narrow ? 2 : 1
                Layout.row: 0
                Layout.column: layout.narrow ? 0 : 1
                Layout.fillWidth: true
                elide: Text.ElideRight
            }

            Label {
                id: descriptionLabel
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.row: 1
                Layout.column: 1
                maximumLineCount: root.expanded ? -1 : 2
                elide: Text.ElideRight
                wrapMode: Text.WordWrap
                textFormat: Text.RichText
                clip: true

                Rectangle {
                    anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
                    height: parent.height / 2
                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "transparent"}
                        GradientStop { position: 1.0; color: root.background.color}
                    }
                    visible: dots.visible
                }
                Label {
                    id: dots
                    anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
                    text: "..."
                    font.pointSize: 30
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignBottom
                    visible: parent.textFormat == Text.RichText ?
                        (parent.height < parent.implicitHeight) : parent.truncated
                }
            }

            RowLayout {
                id: bottomLayout
                Layout.columnSpan: layout.narrow ? 2 : 1
                Layout.row: 2
                Layout.column: layout.narrow ? 0 : 1
                Layout.fillWidth: true

                Rectangle {
                    radius: 8
                    implicitWidth: durationLabel.implicitWidth + 16
                    implicitHeight: durationLabel.implicitHeight + 4
                    color: "#333"
                    visible: root.duration > 0
                    Label {
                        id: durationLabel
                        anchors { fill: parent; leftMargin: 8; rightMargin: 8 }
                        text: Utils.formatMediaTime(root.duration * 1000)
                        color: "white"
                        font.pointSize: 8
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }

                Label {
                    Layout.fillWidth: true
                    text: root.feedUrl ?
                        qsTr("By <a href=\"%2\">%1</a>").arg(root.author).arg(root.feedUrl) :
                        qsTr("By <font color=\"#aaa\">%1</font>").arg(root.author)
                    visible: root.author.length > 0
                    font.pointSize: 8
                    textFormat: Text.RichText
                    elide: Text.ElideRight
                    verticalAlignment: Text.AlignVCenter
                    onLinkActivated: root.openFeedRequested(link)
                    clip: true
                }

                Item { Layout.fillWidth: true }
                Label {
                    id: hostLabel
                    visible: text != ""
                }

                Rectangle {
                    radius: 8
                    Layout.maximumWidth: implicitWidth
                    Layout.fillWidth: true
                    implicitWidth: dateLabel.implicitWidth + 16
                    implicitHeight: dateLabel.implicitHeight + 4
                    color: "#333"
                    visible: dateLabel.text
                    Label {
                        id: dateLabel
                        anchors { fill: parent; leftMargin: 8; rightMargin: 8 }
                        color: "white"
                        font.pointSize: 8
                        elide: Text.ElideRight
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }
        }

        ColumnLayout {
            Layout.fillWidth: false

            Button {
                Layout.fillWidth: true
                Layout.minimumWidth: implicitWidth
                text: qsTr("Play")
                highlighted: true
                onClicked: root.playRequested()
            }

            Button {
                Layout.fillWidth: true
                Layout.minimumWidth: implicitWidth
                visible: root.showMoreInfoButton
                text: qsTr("More…")
                onClicked: root.infoRequested()
            }
        }
    }

    onClicked: expanded = true
}
