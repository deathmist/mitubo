import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Page {
    id: root

    property alias activeDownloadModel: listView.model

    title: qsTr("Active downloads")

    header: TitleHeader {}

    ListView {
        id: listView

        anchors { fill: parent; margins: 8 }

        ScrollBar.vertical: ScrollBar {}

        header: Label {
            property url downloadFolder: Utils.localFileUrl(
                root.activeDownloadModel.downloader.downloadFolderPath)
            width: parent.width
            height: 64
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Open <a href=\"%1\">MiTubo's download folder</a> to see all past downloads").
                arg(downloadFolder)
            textFormat: Text.RichText
            wrapMode: Text.WordWrap
            onLinkActivated: Qt.openUrlExternally(link)
        }

        delegate: DownloadItemDelegate {
            width: ListView.view.width
            download: model.download
            videoData: model.videoData
        }

        Label {
            anchors.centerIn: parent
            text: qsTr("No active downloads")
            font.pointSize: 24
            visible: listView.count == 0
        }
    }
}
