import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

SwipeDelegate {
    id: root

    property string subText: ""
    property bool editable: true

    signal deletionRequested()

    swipe.enabled: editable
    swipe.left: DeleteButton {
        anchors {
            top: parent.top; bottom: parent.bottom
            right: parent.background.left
        }
        onClicked: root.deletionRequested()
    }
    clip: true

    contentItem: ColumnLayout {
        Text {
            rightPadding: root.spacing
            text: root.text
            font: root.font
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }
        Text {
            rightPadding: root.spacing
            text: root.subText
            font {
                family: root.font.family
                pointSize: root.font.pointSize * 2 / 3
            }
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }
    }
}
