/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "youtube_dl.h"

#include "youtube_dl_installer.h"

#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QJSEngine>
#include <QJSValue>
#include <QJsonArray>
#include <QJsonDocument>
#include <QProcess>
#include <QProcessEnvironment>
#include <QQmlEngine>
#include <QSharedPointer>
#include <QStandardPaths>
#include <QString>
#include <QTextStream>
#include <QUrl>
#include <locale.h>

using namespace MiTubo;

namespace MiTubo {

class Download: public QObject
{
    Q_OBJECT
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(QString fileName READ fileName NOTIFY fileNameChanged)
    Q_PROPERTY(QString filePath READ filePath NOTIFY fileNameChanged)
    Q_PROPERTY(QString fileSize READ fileSize NOTIFY progressChanged)
    Q_PROPERTY(qreal progress READ progress NOTIFY progressChanged)
    Q_PROPERTY(QString downloadSpeed READ downloadSpeed NOTIFY progressChanged)
    Q_PROPERTY(QString errorString READ errorString NOTIFY statusChanged)

public:
    enum Status {
        NotStarted = 0,
        Downloading,
        DownloadComplete,
        DownloadFailed,
    };
    Q_ENUM(Status)

    Download(QProcess *downloader, QObject *parent = nullptr);
    ~Download() = default;

    Status status() const { return m_status; }
    QString fileName() const { return m_fileName; }
    QString filePath() const;
    QString fileSize() const { return m_fileSize; }
    qreal progress() const { return m_progress; }
    QString downloadSpeed() const { return m_speed; }
    QString errorString() const { return m_errorString; }

    Q_INVOKABLE void stop();

Q_SIGNALS:
    void statusChanged();
    void fileNameChanged();
    void progressChanged();

private:
    void setStatus(Status status);
    static QString extractError(const QByteArray &errorOutput);
    void onReadyRead();
    void onFinished(int exitCode, QProcess::ExitStatus exitStatus);

private:
    QScopedPointer<QProcess> m_downloader;
    Status m_status = NotStarted;
    QString m_fileName;
    qreal m_progress = 0.0;
    QString m_fileSize;
    QString m_speed;
    QString m_errorString;
};

Download::Download(QProcess *downloader, QObject *parent):
    QObject(parent),
    m_downloader(downloader)
{
    QObject::connect(downloader, &QProcess::readyReadStandardOutput,
                     this, &Download::onReadyRead);
    QObject::connect(downloader,
        qOverload<int,QProcess::ExitStatus>(&QProcess::finished),
        this, &Download::onFinished);
    downloader->start();
}

void Download::setStatus(Status status)
{
    m_status = status;
    Q_EMIT statusChanged();
}

void Download::onReadyRead()
{
    static const QByteArray downloadPrefix = QByteArrayLiteral("[download] ");
    static const QByteArray destinationPrefix = QByteArrayLiteral("Destination: ");

    setlocale(LC_NUMERIC, "C");
    while (m_downloader->canReadLine()) {
        QByteArray line = m_downloader->readLine();
        if (!line.startsWith(downloadPrefix)) continue;
        if (m_status != Downloading) {
            setStatus(Downloading);
        }
        line = line.mid(downloadPrefix.length());
        if (line.startsWith(destinationPrefix)) {
            m_fileName = line.mid(destinationPrefix.length()).trimmed();
            Q_EMIT fileNameChanged();
        } else {
            // download progress
            char sizeBuf[21];
            char speedBuf[21];
            int numParsed = sscanf(line.constData(), "%lf%% of %20s at %20s",
                                   &m_progress, sizeBuf, speedBuf);
            if (numParsed >= 1) {
                if (numParsed >= 3) {
                    m_fileSize = QString::fromUtf8(sizeBuf);
                    m_speed = QString::fromUtf8(speedBuf);
                }
                Q_EMIT progressChanged();
            }
        }
    }
    setlocale(LC_NUMERIC, "");
}

QString Download::extractError(const QByteArray &errorOutput)
{
    static const QString errorPrefix = QStringLiteral("ERROR: ");

    QTextStream ts(errorOutput, QIODevice::ReadOnly);

    QString error;
    QString line;
    while (ts.readLineInto(&line)) {
        if (!line.startsWith(errorPrefix)) continue;

        if (!error.isEmpty()) error += ' ';
        error.append(line.midRef(errorPrefix.length()));
    }
    return error;
}

void Download::onFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    QByteArray errorOutput = m_downloader->readAllStandardError();
    qDebug() << "Downloader finished. Stderr:" << errorOutput;
    m_errorString = extractError(errorOutput);

    bool ok = (exitStatus == QProcess::NormalExit && exitCode == 0);
    setStatus(ok ? DownloadComplete : DownloadFailed);
}

QString Download::filePath() const
{
    if (m_fileName.isEmpty()) return QString();
    return QDir(m_downloader->workingDirectory()).filePath(m_fileName);
}

void Download::stop() {
    m_downloader->terminate();

    // On Windows, terminate() does not affect console applications
#ifdef Q_OS_WIN
    m_downloader->kill();
#endif

    // This will block the UI, so don't wait for more than 2 seconds
    m_downloader->waitForFinished(2000);
    if (!m_fileName.isEmpty()) {
        QString partFileName = m_fileName + QStringLiteral(".part");
        QDir(m_downloader->workingDirectory()).remove(partFileName);
    }
}

class YoutubeDlPrivate {
public:
    YoutubeDlPrivate();

    QProcess *createDownloader();

    Download *download(const QUrl &url, const QJsonObject &format);
    void ensureDownloadDir() const;

    static QJsonObject parseReply(const QByteArray &output);

public:
    QString m_appName;
    QString m_programPath;
    QProcessEnvironment m_env;
    mutable QDir m_downloadDir;
};

} // namespace

YoutubeDlPrivate::YoutubeDlPrivate():
    m_env(QProcessEnvironment::systemEnvironment())
{
    qmlRegisterUncreatableType<Download>("MiTubo", 1, 0, "Download",
                                         "Cannot be created from QML");
    m_env.insert(
         "XDG_CACHE_HOME",
         QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
}

QProcess *YoutubeDlPrivate::createDownloader()
{
    QProcess *downloader = new QProcess();
    downloader->setProgram("python3");
    downloader->setArguments({ m_programPath });
    downloader->setProcessEnvironment(m_env);
    return downloader;
}

Download *YoutubeDlPrivate::download(const QUrl &url,
                                     const QJsonObject &format)
{
    ensureDownloadDir();

    QProcess *downloader = createDownloader();
    QString formatId = format.value("format_id").toString();
    downloader->setArguments(downloader->arguments() + QStringList {
        "--no-check-certificate",
        "--no-continue",
        "--newline",
        "-f", formatId,
        url.toString(),
    });
    downloader->setWorkingDirectory(m_downloadDir.absolutePath());
    return new Download(downloader);
}

void YoutubeDlPrivate::ensureDownloadDir() const
{
    m_downloadDir.setPath(
        QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
    if (!m_appName.isEmpty() && m_downloadDir.mkpath(m_appName)) {
        m_downloadDir.cd(m_appName);
    } else {
        /* We don't have permissions to use the global download folder: use the
         * application's data folder instead. */
        m_downloadDir.setPath(
            QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
        const QString downloads = QStringLiteral("Downloads");
        if (m_downloadDir.mkpath(downloads)) {
            m_downloadDir.cd(downloads);
        }
    }
}

QJsonObject YoutubeDlPrivate::parseReply(const QByteArray &output)
{
    const auto lines = output.trimmed().split('\n');
    if (lines.count() > 1) {
        qDebug() << "URL refers to a playlist; item count:" << lines.count();
        QJsonArray items;
        for (const QByteArray &line: lines) {
            items.append(QJsonDocument::fromJson(line).object());
        }
        const QJsonObject first = items.first().toObject();
        return QJsonObject {
            { "isPlaylist", true },
            {
                "uploader",
                first[QLatin1String("playlist_uploader")].toString()
            },
            { "name", first[QLatin1String("playlist_title")].toString() },
            { "items", items },
        };
    } else {
        return QJsonDocument::fromJson(output).object();
    }
}

YoutubeDl::YoutubeDl(QObject *parent):
    QObject(parent),
    d_ptr(new YoutubeDlPrivate())
{
    checkIsInstalled();
}

YoutubeDl::~YoutubeDl()
{
}

void YoutubeDl::setApplicationName(const QString &name)
{
    Q_D(YoutubeDl);
    d->m_appName = name;
    Q_EMIT applicationNameChanged();
}

QString YoutubeDl::applicationName() const
{
    Q_D(const YoutubeDl);
    return d->m_appName;
}

bool YoutubeDl::isInstalled() const
{
    Q_D(const YoutubeDl);
    return !d->m_programPath.isEmpty();
}

QString YoutubeDl::downloadFolderPath() const
{
    Q_D(const YoutubeDl);
    d->ensureDownloadDir();
    return d->m_downloadDir.absolutePath();
}

void YoutubeDl::checkIsInstalled()
{
    Q_D(YoutubeDl);
    bool wasInstalled = isInstalled();
    d->m_programPath = QStandardPaths::findExecutable("youtube-dl");
    if (d->m_programPath.isEmpty()) {
        /* On Windows, python scripts like youtube-dl are not recognized as
         * executables, therefore findExecutable() fails there. Then, just
         * check if we have downloaded our own copy.
         */
        QDir executableDir(YoutubeDlInstaller::executableDir());
        if (executableDir.exists("youtube-dl")) {
            d->m_programPath = executableDir.absoluteFilePath("youtube-dl");
        }
    }
    qDebug() << "Program found:" << d->m_programPath;
    if (isInstalled() != wasInstalled) {
        Q_EMIT isInstalledChanged();
    }
}

void YoutubeDl::getUrlInfo(const QUrl &url, const GetUrlInfoCb &callback)
{
    Q_D(YoutubeDl);

    QProcess *downloader = d->createDownloader();

    QJsonObject json;
    qDebug() << Q_FUNC_INFO << url;

    QObject::connect(downloader,
        qOverload<int,QProcess::ExitStatus>(&QProcess::finished),
        this,
        [downloader, url, callback]
        (int exitCode, QProcess::ExitStatus exitStatus) {
        downloader->deleteLater();
        QByteArray errorOutput = downloader->readAllStandardError();
        qDebug() << errorOutput;
        QJsonObject result;
        bool ok = (exitStatus == QProcess::NormalExit && exitCode == 0);
        if (ok) {
            QByteArray output = downloader->readAllStandardOutput();
            result = YoutubeDlPrivate::parseReply(output);
        } else {
            qWarning() << "Downloader error:" << downloader->error();
            result = QJsonObject {
                { "error", QString::fromUtf8(errorOutput) },
                { "errorCode", downloader->error() },
            };
        }
        callback(result);
    });

    downloader->setArguments(downloader->arguments() + QStringList {
        "--no-check-certificate",
        "-j",
        url.toString(),
    });
    downloader->start();
}

void YoutubeDl::getUrlInfo(const QUrl &url, const QJSValue &callback)
{
    getUrlInfo(url, [this, callback](const QJsonObject &result) {
        QJSValue cbCopy(callback); // needed as callback is captured as const
        QJSEngine *engine = qjsEngine(this);
        cbCopy.call(QJSValueList { engine->toScriptValue(result) });
    });
}

QObject *YoutubeDl::download(const QUrl &url, const QJsonObject &format)
{
    Q_D(YoutubeDl);
    Download *dl = d->download(url, format);
    QQmlEngine::setObjectOwnership(dl, QQmlEngine::CppOwnership);
    return static_cast<QObject*>(dl);
}

#include "youtube_dl.moc"
