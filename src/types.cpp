/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"

#include "feed_discoverer.h"
#include "feed_item_model.h"
#include "feed_model.h"
#include "playlist.h"
#include "playlist_model.h"
#include "request_dispatcher.h"
#include "settings.h"
#include "updater.h"
#include "utils.h"
#include "video_extractor.h"
#include "youtube_dl.h"
#include "youtube_dl_installer.h"

#include <QByteArray>
#include <QDebug>
#include <QQmlEngine>

#include <ScreenSaver/Inhibitor>

using namespace MiTubo;

void MiTubo::registerTypes()
{
    qmlRegisterType<Mardy::Updater>("Mardy", 1, 0, "Updater");

    qmlRegisterType<FeedDiscoverer>("MiTubo", 1, 0, "FeedDiscoverer");
    qmlRegisterType<FeedItemModel>("MiTubo", 1, 0, "FeedItemModel");
    qmlRegisterType<FeedModel>("MiTubo", 1, 0, "FeedModel");
    qmlRegisterUncreatableType<Playlist>("MiTubo", 1, 0, "Playlist",
                                         "Cannot be created from QML");
    qmlRegisterType<PlaylistModel>("MiTubo", 1, 0, "PlaylistModel");
    qmlRegisterType<it::mardy::ScreenSaver::Inhibitor>("MiTubo", 1, 0,
                                                       "ScreenSaverInhibitor");
    qmlRegisterType<YoutubeDl>("MiTubo", 1, 0, "YoutubeDl");
    qmlRegisterType<VideoExtractor>("MiTubo", 1, 0, "VideoExtractor");
#ifdef Q_OS_UNIX
    {
        QByteArray path = qgetenv("PATH");
        path.prepend(YoutubeDlInstaller::executableDir().toUtf8() + ':');
        qputenv("PATH", path);
    }
#endif
    qmlRegisterType<YoutubeDlInstaller>("MiTubo", 1, 0, "YoutubeDlInstaller");
    qmlRegisterSingletonType<RequestDispatcher>(
        "MiTubo", 1, 0, "RequestDispatcher",
        [](QQmlEngine *, QJSEngine *) -> QObject * {
            return new RequestDispatcher();
        });
    qmlRegisterSingletonType<Settings>(
        "MiTubo", 1, 0, "Settings",
        [](QQmlEngine *, QJSEngine *) -> QObject * {
            return new Settings();
        });
    qmlRegisterSingletonType<Utils>(
        "MiTubo", 1, 0, "Utils",
        [](QQmlEngine *, QJSEngine *) -> QObject * {
            return new Utils();
        });
}
