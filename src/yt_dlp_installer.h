/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_YT_DLP_INSTALLER_H
#define MITUBO_YT_DLP_INSTALLER_H

#include "abstract_github_installer.h"

class QDir;

namespace MiTubo {

/*
 * The yt-dlp downloader
 *
 * https://github.com/yt-dlp/yt-dlp
 */
class YtDlpInstaller: public AbstractGithubInstaller {
public:
    YtDlpInstaller();
    void applyPatches(const QString &archivePath) override;

private:
    void patchCompatFile(const QDir &archiveDir);
    void patchMainFile(const QDir &archiveDir);
};

} // namespace

#endif // MITUBO_YT_DLP_INSTALLER_H
