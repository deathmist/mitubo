/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_VIDEO_EXTRACTOR_H
#define MITUBO_VIDEO_EXTRACTOR_H

#include <QObject>
#include <QScopedPointer>

class QJSValue;
class QUrl;

namespace MiTubo {

class VideoExtractorPrivate;
class VideoExtractor: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QObject *youtubeDl READ youtubeDl WRITE setYoutubeDl
               NOTIFY youtubeDlChanged)

public:
    VideoExtractor(QObject *parent = nullptr);
    virtual ~VideoExtractor();

    void setYoutubeDl(QObject *youtubeDl);
    QObject *youtubeDl() const;

    Q_INVOKABLE void getUrlInfo(const QUrl &url, const QJSValue &callback);

Q_SIGNALS:
    void youtubeDlChanged();

private:
    Q_DECLARE_PRIVATE(VideoExtractor)
    QScopedPointer<VideoExtractorPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_VIDEO_EXTRACTOR_H
