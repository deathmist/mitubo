/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include <QDebug>
#include <QRegularExpression>
#include <QRegularExpressionMatchIterator>

using namespace MiTubo;

Utils::Utils(QObject *parent):
    QObject(parent)
{
}

Utils::~Utils() = default;

QString Utils::formatMediaTime(int milliseconds) const
{
	int totalSeconds = milliseconds / 1000;
	int totalMinutes = totalSeconds / 60;
	int seconds = totalSeconds % 60;
	int hours = totalMinutes / 60;
	int minutes = totalMinutes % 60;

	return (hours > 0 ?
			QString("%1:%2:%3").arg(hours) :
			QString("%1:%2")).
		arg(minutes, 2, 10, QChar('0')).arg(seconds, 2, 10, QChar('0'));
}

QStringList Utils::extractUrls(const QString &text) const
{
    static QRegularExpression urlRe("(https?://[-A-Z0-9+&@#/%?=~_|!:,.;]*[A-Z0-9+&@#/%=~_|])",
                                    QRegularExpression::CaseInsensitiveOption);
    QStringList ret;
    auto i = urlRe.globalMatch(text);
    while (i.hasNext()) {
        const QRegularExpressionMatch m = i.next();
        ret.append(m.captured());
    }
    return ret;
}

QUrl Utils::localFileUrl(const QString &path) const
{
    return QUrl::fromLocalFile(path);
}
