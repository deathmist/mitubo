/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "youtube_dl_installer.h"

#include "abstract_github_installer.h"
#include "yt_dlp_installer.h"

#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QProcess>
#include <QQmlEngine>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QTemporaryFile>
#include <QUrl>
#include <QVersionNumber>

using namespace MiTubo;

namespace MiTubo {

QString AbstractInstaller::installedVersion(const QString &archivePath) const
{
    QDir baseDir(archivePath);
    QString ret;

    QDir packageDir(baseDir);
    packageDir.cd(m_programDir);

    QFile versionFile(packageDir.filePath("version.py"));
    bool ok = versionFile.open(QIODevice::ReadOnly);
    if (!ok) return ret;

    QString contents = QString::fromUtf8(versionFile.readAll());
    QRegularExpression re(R"re(__version__ *= *['"]([0-9.]*))re",
                          QRegularExpression::MultilineOption);
    QRegularExpressionMatch match = re.match(contents);
    return match.hasMatch() ? match.captured(1) : ret;
}

class YoutubeDlOrgInstaller: public AbstractGithubInstaller {
public:
    YoutubeDlOrgInstaller();
    void applyPatches(const QString &archivePath) override;

private:
    void patchCompatFile(const QDir &archiveDir);
    void patchMainFile(const QDir &archiveDir);
};

YoutubeDlOrgInstaller::YoutubeDlOrgInstaller():
    AbstractGithubInstaller(QStringLiteral("ytdl-patched/youtube-dl"))
{
    m_programName = QStringLiteral("youtube-dl");
    m_programDir = QStringLiteral("youtube_dl");
}

void YoutubeDlOrgInstaller::applyPatches(const QString &archivePath)
{
    QDir archiveDir(archivePath);

    patchCompatFile(archiveDir);
    patchMainFile(archiveDir);

    /* Rename the main file */
    archiveDir.rename("__main__.py", "youtube-dl");
}

void YoutubeDlOrgInstaller::patchCompatFile(const QDir &archiveDir)
{
    /* We need to remove the compat_http_server, which is used only in
     * youtube-dl testing framework and causes a startup failure in Ubuntu
     * Touch due to confinement. */
    QFile compatFile(archiveDir.filePath("youtube_dl/compat.py"));
    bool ok = compatFile.open(QIODevice::ReadWrite);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not open youtube_dl/compat.py for patching";
        return;
    }
    QByteArray contents = compatFile.readAll();
    contents.replace("import http.server as compat_http_server", "pass");
    contents.replace("import BaseHTTPServer as compat_http_server", "pass");
    compatFile.resize(0);
    compatFile.write(contents);
}

void YoutubeDlOrgInstaller::patchMainFile(const QDir &archiveDir)
{
    /* Correct the shebang line */
    QFile mainFile(archiveDir.filePath("__main__.py"));
    bool ok = mainFile.open(QIODevice::ReadWrite);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not open youtube_dl/main.py for patching";
        return;
    }

    QString pythonPath = QStandardPaths::findExecutable("python3");
    QByteArray newShebangLine = "#! " + pythonPath.toUtf8();
    QByteArray contents = mainFile.readAll();
    int lineEnd = contents.indexOf('\n');
    contents.replace(0, lineEnd, newShebangLine);
    mainFile.resize(0);
    mainFile.write(contents);
}

class YoutubeDlInstallerPrivate
{
    Q_DECLARE_PUBLIC(YoutubeDlInstaller)

public:
    YoutubeDlInstallerPrivate(YoutubeDlInstaller *q);

    static AbstractInstaller *installerFactory();

    bool prepareDownloadFile();
    void saveData(QIODevice *network);

    void extractLatestVersion();
    void activateLatestVersion();

private:
    QScopedPointer<AbstractInstaller> m_installer;
    YoutubeDlInstaller::Status m_status;
    QString m_currentVersion;
    QString m_latestVersion;
    bool m_autoDownload = false;
    bool m_autoInstall = true;
    int64_t m_bytesReceived = 0;
    int64_t m_bytesTotal = 0;
    static constexpr int m_maxDownloadSize = 5 * 1024 * 1024;
    QScopedPointer<QTemporaryFile> m_tmpFile;
    QDir m_executableDir;
    QProcess m_extractor;
    YoutubeDlInstaller *q_ptr;
};

} // namespace

static const QString ActiveDir = QStringLiteral("downloader");
static const QString ExtractionDir = QStringLiteral("downloader-latest");
static const QString ObsoleteDir = QStringLiteral("downloader-old");

YoutubeDlInstallerPrivate::YoutubeDlInstallerPrivate(YoutubeDlInstaller *q):
    m_installer(installerFactory()),
    m_status(YoutubeDlInstaller::UnknownStatus),
    m_currentVersion(
        m_installer->installedVersion(YoutubeDlInstaller::executableDir())),
    q_ptr(q)
{
    m_extractor.setProgram("unzip");
    m_executableDir.setPath(YoutubeDlInstaller::executableDir());
    m_executableDir.mkpath(".");

    QObject::connect(&m_extractor,
                     qOverload<int,QProcess::ExitStatus>(&QProcess::finished),
                     [this, q](int exitCode, QProcess::ExitStatus exitStatus) {
        qDebug() << "process error output" << m_extractor.readAllStandardError();
        // Remove the downloaded file
        m_tmpFile.reset();
        bool ok = (exitStatus == QProcess::NormalExit && exitCode == 0);
        if (ok) {
            activateLatestVersion();
        } else {
            q->setStatus(YoutubeDlInstaller::InstallationFailed);
        }
    });
}

AbstractInstaller *YoutubeDlInstallerPrivate::installerFactory()
{
    QProcess python;
    python.start("python3", { "--version" });
    python.waitForFinished();
    QByteArray output = python.readAllStandardOutput();
    QByteArray versionData = output.split(' ').last();
    QVersionNumber version =
        QVersionNumber::fromString(QString::fromUtf8(versionData));
    qDebug() << "Python version:" << version;
    if (version >= QVersionNumber(3, 6)) {
        return new YtDlpInstaller();
    } else {
        return new YoutubeDlOrgInstaller;
    }
}

bool YoutubeDlInstallerPrivate::prepareDownloadFile()
{
    QString templateName = QDir::tempPath() + "/youtube-dl_XXXXXX.zip";
    m_tmpFile.reset(new QTemporaryFile(templateName));
    bool ok = m_tmpFile->open();
    qDebug() << "Writing to" << m_tmpFile->fileName();
    return ok;
}

void YoutubeDlInstallerPrivate::saveData(QIODevice *network)
{
    Q_Q(YoutubeDlInstaller);
    qint64 availableStorage = m_maxDownloadSize - m_tmpFile->pos();
    if (availableStorage <= 0) {
        qWarning() << "Download file exceeds maximum size:"
            << m_tmpFile->pos();
        q->setStatus(YoutubeDlInstaller::DownloadFailed);
        return;
    }

    QByteArray data = network->read(availableStorage);
    /* The downloaded file starts with a shebang line which confuses unzip: it
     * still processes the archive, but returns 1 as exit code. So, let's
     * remove it. */
    if (m_tmpFile->size() == 0) {
        int endOfFirstLine = data.indexOf("\nPK");
        if (endOfFirstLine > 0) {
            data = data.mid(endOfFirstLine + 1);
        }
    }
    int readCount = data.length();
    int64_t writtenCount = m_tmpFile->write(data);
    if (Q_UNLIKELY(writtenCount != readCount)) {
        qWarning() << "read" << readCount << "but wrote only" << writtenCount;
    }
}

void YoutubeDlInstallerPrivate::extractLatestVersion()
{
    qDebug() << "extracting";
    QDir baseDir(m_executableDir);
    baseDir.cdUp();

#ifdef Q_OS_WIN
    /* Workaround QTemporaryFile not being ready (unzip doesn't see it).
     * flushing and calling sleep() does not help.
     */
    QString dummyFileName;
    {
        QTemporaryFile dummy;
        if (dummy.open()) {
            dummyFileName = dummy.fileName();
            qDebug() << "prepared dummy file" << dummyFileName;
            dummy.close();
        }
    }
    bool ok = m_tmpFile->copy(dummyFileName);
    qDebug() << "Temporary copy succeded:" << ok << m_tmpFile->error() << m_tmpFile->errorString();
#endif

    /* Remove any leftovers from previous failed installations */
    QStringList temporaryDirs = { ExtractionDir, ObsoleteDir };
    for (const QString &dir: temporaryDirs) {
        if (baseDir.cd(dir)) {
            baseDir.removeRecursively();
            baseDir.cdUp();
        }
    }

    baseDir.mkpath(ExtractionDir);
    QString extractionDir = baseDir.filePath(ExtractionDir);
    m_extractor.setWorkingDirectory(extractionDir);
    m_extractor.setArguments({
        m_tmpFile->fileName(),
    });
    m_extractor.start();
}

void YoutubeDlInstallerPrivate::activateLatestVersion()
{
    Q_Q(YoutubeDlInstaller);
    qDebug() << "activating";
    QDir baseDir(m_executableDir);
    baseDir.cdUp();

    m_installer->applyPatches(baseDir.filePath(ExtractionDir));

    baseDir.rename(ActiveDir, ObsoleteDir);
    baseDir.rename(ExtractionDir, ActiveDir);
    if (baseDir.cd(ObsoleteDir)) {
        baseDir.removeRecursively();
    }

    q->setStatus(YoutubeDlInstaller::InstallationComplete);
}

YoutubeDlInstaller::YoutubeDlInstaller(QObject *parent):
    QObject(parent),
    d_ptr(new YoutubeDlInstallerPrivate(this))
{
    QObject::connect(this, &YoutubeDlInstaller::statusChanged,
                     [this]() {
        if (status() == UpdateAvailable && autoDownload()) {
            downloadLatestVersion();
        } else if (status() == DownloadComplete && autoInstall()) {
            installLatestVersion();
        }
    });
}

YoutubeDlInstaller::~YoutubeDlInstaller() = default;

QString YoutubeDlInstaller::executableDir()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) +
        "/" + ActiveDir;
}

void YoutubeDlInstaller::setStatus(Status status)
{
    Q_D(YoutubeDlInstaller);

    if (status == d->m_status) return;
    d->m_status = status;
    Q_EMIT statusChanged();
}

YoutubeDlInstaller::Status YoutubeDlInstaller::status() const
{
    Q_D(const YoutubeDlInstaller);
    return d->m_status;
}

QString YoutubeDlInstaller::programName() const
{
    Q_D(const YoutubeDlInstaller);
    return d->m_installer->programName();
}

void YoutubeDlInstaller::setCurrentVersion(const QString &currentVersion)
{
    Q_D(YoutubeDlInstaller);

    if (currentVersion == d->m_currentVersion) return;
    d->m_currentVersion = currentVersion;
    Q_EMIT currentVersionChanged();
}

QString YoutubeDlInstaller::currentVersion() const
{
    Q_D(const YoutubeDlInstaller);
    return d->m_currentVersion;
}

QString YoutubeDlInstaller::latestVersion() const
{
    Q_D(const YoutubeDlInstaller);
    return d->m_latestVersion;
}

void YoutubeDlInstaller::setAutoDownload(bool autoDownload)
{
    Q_D(YoutubeDlInstaller);

    if (autoDownload == d->m_autoDownload) return;
    d->m_autoDownload = autoDownload;
    Q_EMIT autoDownloadChanged();
}

bool YoutubeDlInstaller::autoDownload() const
{
    Q_D(const YoutubeDlInstaller);
    return d->m_autoDownload;
}

void YoutubeDlInstaller::setAutoInstall(bool autoInstall)
{
    Q_D(YoutubeDlInstaller);

    if (autoInstall == d->m_autoInstall) return;
    d->m_autoInstall = autoInstall;
    Q_EMIT autoInstallChanged();
}

bool YoutubeDlInstaller::autoInstall() const
{
    Q_D(const YoutubeDlInstaller);
    return d->m_autoInstall;
}

void YoutubeDlInstaller::setBytesReceived(int64_t bytesReceived)
{
    Q_D(YoutubeDlInstaller);
    d->m_bytesReceived = bytesReceived;
    Q_EMIT bytesReceivedChanged();
}

int64_t YoutubeDlInstaller::bytesReceived() const
{
    Q_D(const YoutubeDlInstaller);
    return d->m_bytesReceived;
}

void YoutubeDlInstaller::setBytesTotal(int64_t bytesTotal)
{
    Q_D(YoutubeDlInstaller);
    if (bytesTotal == d->m_bytesTotal) return;
    d->m_bytesTotal = bytesTotal;
    Q_EMIT bytesTotalChanged();
}

int64_t YoutubeDlInstaller::bytesTotal() const
{
    Q_D(const YoutubeDlInstaller);
    return d->m_bytesTotal;
}

void YoutubeDlInstaller::checkForUpdates()
{
    Q_D(YoutubeDlInstaller);
    setStatus(CheckingForUpdates);

    QQmlEngine *engine = qmlEngine(this);
    Q_ASSERT(engine);
    QNetworkAccessManager *nam = engine->networkAccessManager();
    Q_ASSERT(nam);

    QNetworkRequest req(d->m_installer->versionUrl());
    req.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
                     QNetworkRequest::NoLessSafeRedirectPolicy);

    QNetworkReply *reply = nam->get(req);
    reply->ignoreSslErrors();
    QObject::connect(reply,
        qOverload<QNetworkReply::NetworkError>(&QNetworkReply::error),
        this, [this, reply](QNetworkReply::NetworkError) {
        qDebug() << "Network error:" << reply->errorString();
        setStatus(CheckForUpdatesFailed);
    });
    QObject::connect(reply, &QNetworkReply::finished,
                     this, [this, reply]() {
        Q_D(YoutubeDlInstaller);
        reply->deleteLater();

        QByteArray contents = reply->readAll();
        d->m_latestVersion = d->m_installer->parseLatestVersion(contents);
        qDebug() << "Version reply:" << d->m_latestVersion;
        setStatus(d->m_latestVersion > currentVersion() ?
                  UpdateAvailable : NoUpdatesAvailable);
    });
}

void YoutubeDlInstaller::downloadLatestVersion()
{
    Q_D(YoutubeDlInstaller);
    setStatus(Downloading);

    if (!d->prepareDownloadFile()) {
        setStatus(DownloadFailed);
        return;
    }

    QQmlEngine *engine = qmlEngine(this);
    Q_ASSERT(engine);
    QNetworkAccessManager *nam = engine->networkAccessManager();
    Q_ASSERT(nam);

    QNetworkRequest req(d->m_installer->downloadUrl());
    req.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
                     QNetworkRequest::NoLessSafeRedirectPolicy);

    QNetworkReply *reply = nam->get(req);
    reply->ignoreSslErrors();
    QObject::connect(reply, &QNetworkReply::downloadProgress,
                     this, [this](qint64 bytesReceived, qint64 bytesTotal) {
        setBytesReceived(bytesReceived);
        setBytesTotal(bytesTotal);
    });
    QObject::connect(reply, &QIODevice::readyRead,
                     this, [d, reply]() { d->saveData(reply); });
    QObject::connect(reply,
        qOverload<QNetworkReply::NetworkError>(&QNetworkReply::error),
        this, [this, reply](QNetworkReply::NetworkError) {
        qDebug() << "Network error:" << reply->errorString();
        setStatus(DownloadFailed);
    });
    QObject::connect(reply, &QNetworkReply::finished,
                     this, [this, d, reply]() {
        reply->deleteLater();

        d->saveData(reply);
        d->m_tmpFile->close();
        // Had an error occurred, the failed status would have been set by now
        if (status() != DownloadFailed) {
            setStatus(DownloadComplete);
        }
    });
}

void YoutubeDlInstaller::installLatestVersion()
{
    Q_D(YoutubeDlInstaller);
    setStatus(Installing);
    d->extractLatestVersion();
}
