/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "yt_dlp_installer.h"

#include <QDebug>
#include <QDir>
#include <QStandardPaths>

using namespace MiTubo;

YtDlpInstaller::YtDlpInstaller():
    AbstractGithubInstaller(QStringLiteral("yt-dlp/yt-dlp"))
{
    m_programName = QStringLiteral("yt-dlp");
    m_programDir = QStringLiteral("yt_dlp");
}

void YtDlpInstaller::applyPatches(const QString &archivePath)
{
    QDir archiveDir(archivePath);

    patchCompatFile(archiveDir);
    patchMainFile(archiveDir);

    /* Rename the main file */
    archiveDir.rename("__main__.py", "youtube-dl");

    QFile::setPermissions(archiveDir.filePath("youtube-dl"),
                          QFileDevice::ReadOwner |
                          QFileDevice::WriteOwner |
                          QFileDevice::ExeOwner |
                          QFileDevice::ReadUser |
                          QFileDevice::WriteUser |
                          QFileDevice::ExeUser |
                          QFileDevice::ReadGroup |
                          QFileDevice::ExeGroup |
                          QFileDevice::ReadOther |
                          QFileDevice::ExeOther);
}

void YtDlpInstaller::patchCompatFile(const QDir &archiveDir)
{
    /* We need to remove the compat_http_server, which is used only in
     * youtube-dl testing framework and causes a startup failure in Ubuntu
     * Touch due to confinement. */
    QFile compatFile(archiveDir.filePath("yt_dlp/compat.py"));
    bool ok = compatFile.open(QIODevice::ReadWrite);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not open youtube_dl/compat.py for patching";
        return;
    }
    QByteArray contents = compatFile.readAll();
    contents.replace("\nimport http.server", "\n#import http.server");
    contents.replace("compat_http_server = http.server", "compat_http_server = None");
    compatFile.resize(0);
    compatFile.write(contents);
}

void YtDlpInstaller::patchMainFile(const QDir &archiveDir)
{
    /* Correct the shebang line */
    QFile mainFile(archiveDir.filePath("__main__.py"));
    bool ok = mainFile.open(QIODevice::ReadWrite);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not open youtube_dl/main.py for patching";
        return;
    }

    QString pythonPath = QStandardPaths::findExecutable("python3");
    QByteArray newShebangLine = "#! " + pythonPath.toUtf8();
    QByteArray contents = mainFile.readAll();
    int lineEnd = contents.indexOf('\n');
    contents.replace(0, lineEnd, newShebangLine);
    mainFile.resize(0);
    mainFile.write(contents);
}
