/*
 * Copyright (C) 2021-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_ABSTRACT_INSTALLER_H
#define MITUBO_ABSTRACT_INSTALLER_H

#include <QByteArray>
#include <QUrl>
#include <QString>

namespace MiTubo {

class AbstractInstaller {
public:
    virtual ~AbstractInstaller() = default;

    QString programName() const { return m_programName; }
    QString programDir() const { return m_programDir; }
    QUrl versionUrl() const { return m_versionUrl; }
    QUrl downloadUrl() const { return m_downloadUrl; }

    virtual QString parseLatestVersion(const QByteArray &contents) = 0;
    virtual QString installedVersion(const QString &archivePath) const;
    virtual void applyPatches(const QString &archivePath) = 0;

protected:
    QString m_programName;
    QString m_programDir;
    QUrl m_versionUrl;
    QUrl m_downloadUrl;
};

} // namespace

#endif // MITUBO_ABSTRACT_INSTALLER_H
