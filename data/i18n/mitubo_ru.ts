<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="ia">
<context>
    <name>AboutPopup</name>
    <message>
        <location filename="../../src/desktop/qml/AboutPopup.qml" line="42"/>
        <source>MiTubo version %1&lt;br/&gt;&lt;font size=&quot;1&quot;&gt;Copyright ⓒ 2020-2022 mardy.it&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/AboutPopup.qml" line="50"/>
        <source>MiTubo is free software, distributed under the &lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.en.html&quot;&gt;GPL-v3 license&lt;/a&gt;. Among other things, this means that you can share it with your friends for free, or resell it for a fee.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/AboutPopup.qml" line="61"/>
        <source>If you got MiTubo for free, please consider making a small donation to its author:&lt;br/&gt;&lt;ul&gt;&lt;li&gt;Paypal: &lt;a href=&quot;https://www.paypal.com/donate/?hosted_button_id=P7YTMKGGCD9JC&quot;&gt;click here to donate a free amount&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;a href=&quot;https://money.yandex.ru/quickpay/shop-widget?writer=seller&amp;targets=MiTubo&amp;targets-hint=&amp;default-sum=&amp;button-text=14&amp;hint=&amp;successURL=&amp;quickpay=shop&amp;account=410015419471966&quot;&gt;Yandex Money&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <location filename="../../src/desktop/qml/BackButton.qml" line="5"/>
        <source>❮</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteButton</name>
    <message>
        <location filename="../../src/desktop/qml/DeleteButton.qml" line="4"/>
        <location filename="../../src/ubuntu-touch/qml/DeleteButton.qml" line="9"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadItem</name>
    <message>
        <location filename="../../src/desktop/qml/DownloadItem.qml" line="28"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Open file&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadItem.qml" line="41"/>
        <source>🗙 Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadItem.qml" line="60"/>
        <source>S: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadPage</name>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="14"/>
        <source>Download media file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="35"/>
        <source>Media format selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="42"/>
        <source>Download streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="53"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="68"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloaderErrorPopup</name>
    <message>
        <location filename="../../src/desktop/qml/DownloaderErrorPopup.qml" line="10"/>
        <source>Error opening video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderErrorPopup.qml" line="23"/>
        <source>There was an error retrieving the video information:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloaderInstallationPage</name>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="11"/>
        <source>youtube-dl installation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="17"/>
        <source>Checking for updates…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="22"/>
        <source>No download available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="28"/>
        <source>Failed. Please check your network connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="33"/>
        <source>Downloading…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="39"/>
        <source>Download failed. Please check your network connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="45"/>
        <source>Installing…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="50"/>
        <source>Installation complete!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="56"/>
        <source>Installation failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="83"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloaderIntroPage</name>
    <message>
        <location filename="../../src/desktop/qml/DownloaderIntroPage.qml" line="10"/>
        <source>youtube-dl installation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderIntroPage.qml" line="20"/>
        <source>&lt;p&gt;MiTubo relies on the &lt;b&gt;youtube-dl&lt;/b&gt; program to perform the extraction of the video streams.&lt;/p&gt;&lt;p&gt;A working installation of &lt;b&gt;youtube-dl&lt;/b&gt; could not be found on your system. MiTubo will now start the installation process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderIntroPage.qml" line="30"/>
        <source>Install youtube-dl</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloaderMaintenance</name>
    <message>
        <location filename="../../src/desktop/qml/DownloaderMaintenance.qml" line="25"/>
        <source>A new version (%1) of &lt;b&gt;%2&lt;/b&gt; is available; &lt;a href=&quot;foo&quot;&gt;click here&lt;/a&gt; to install it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderMaintenance.qml" line="33"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadsPage</name>
    <message>
        <location filename="../../src/desktop/qml/DownloadsPage.qml" line="11"/>
        <source>Active downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadsPage.qml" line="29"/>
        <source>Open &lt;a href=&quot;%1&quot;&gt;MiTubo&apos;s download folder&lt;/a&gt; to see all past downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadsPage.qml" line="44"/>
        <source>No active downloads</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FeedContentsOverlay</name>
    <message>
        <location filename="../../src/desktop/qml/FeedContentsOverlay.qml" line="47"/>
        <source>View more items…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FeedDelegate</name>
    <message>
        <location filename="../../src/desktop/qml/FeedDelegate.qml" line="24"/>
        <source>Unsubscribe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FeedPage</name>
    <message>
        <location filename="../../src/desktop/qml/FeedPage.qml" line="17"/>
        <source>RSS feed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FeedPage.qml" line="25"/>
        <source>Subscribed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FeedPage.qml" line="25"/>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormatModels</name>
    <message>
        <location filename="../../src/desktop/qml/FormatModels.qml" line="36"/>
        <source>%1 (bitrate: %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FormatModels.qml" line="49"/>
        <source>Audio + video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FormatModels.qml" line="53"/>
        <source>Audio only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FormatModels.qml" line="57"/>
        <source>Video only</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InvidiousSearch</name>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/InvidiousSearch.qml" line="123"/>
        <source>%n year(s) ago</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/InvidiousSearch.qml" line="125"/>
        <source>%n month(s) ago</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>InvidiousSearchOptions</name>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="28"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="35"/>
        <source>Videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="36"/>
        <source>Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="37"/>
        <source>Playlists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="45"/>
        <source>Published:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="53"/>
        <source>Any time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="54"/>
        <source>Last hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="55"/>
        <source>Today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="56"/>
        <source>Less than a week ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="57"/>
        <source>Less than a month ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="58"/>
        <source>Less than one year ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="65"/>
        <source>Duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="72"/>
        <source>Any</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="73"/>
        <source>Short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="74"/>
        <source>Long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="81"/>
        <source>Sort by:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="88"/>
        <source>Relevance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="89"/>
        <source>Upload date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="90"/>
        <source>Rating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="91"/>
        <source>View count</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinkDropPopup</name>
    <message>
        <location filename="../../src/desktop/qml/LinkDropPopup.qml" line="20"/>
        <source>Link action:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/LinkDropPopup.qml" line="41"/>
        <source>Play now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/LinkDropPopup.qml" line="48"/>
        <source>Add to “Watch later” (first)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/LinkDropPopup.qml" line="54"/>
        <source>Add to “Watch later” (last)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../../src/desktop/qml/MainMenu.qml" line="14"/>
        <source>Downloads…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainMenu.qml" line="23"/>
        <source>Check for updates…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainMenu.qml" line="29"/>
        <source>About MiTubo…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="20"/>
        <source>Video selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="49"/>
        <source>Enter some search terms or the address of the page containing the desired video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="58"/>
        <source>Video address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="76"/>
        <source>Add subscription</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="76"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="76"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="82"/>
        <source>Show info</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/MainPage.qml" line="92"/>
        <source>RSS feed(s) detected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="100"/>
        <source>The following RSS feeds were found in the webpage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="121"/>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="121"/>
        <source>Subscribed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="137"/>
        <source>or</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="144"/>
        <source>Go to your playlists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="154"/>
        <source>Your subscriptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="175"/>
        <source>You have no subscriptions; enter an RSS address in the search box above to add one.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Media</name>
    <message>
        <location filename="../../src/desktop/qml/BufferingLabel.qml" line="8"/>
        <location filename="../../src/ubuntu-touch/qml/BufferingLabel.qml" line="8"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/BufferingLabel.qml" line="9"/>
        <source>Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/BufferingLabel.qml" line="10"/>
        <location filename="../../src/desktop/qml/BufferingLabel.qml" line="11"/>
        <location filename="../../src/ubuntu-touch/qml/BufferingLabel.qml" line="9"/>
        <location filename="../../src/ubuntu-touch/qml/BufferingLabel.qml" line="10"/>
        <source>Buffering</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MiTubo::Playlist</name>
    <message>
        <location filename="../../src/playlist.cpp" line="409"/>
        <source>ContinueWatching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/playlist.cpp" line="413"/>
        <source>WatchHistory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/playlist.cpp" line="417"/>
        <source>WatchLater</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayerErrorPopup</name>
    <message>
        <location filename="../../src/desktop/qml/PlayerErrorPopup.qml" line="13"/>
        <location filename="../../src/desktop/windows/qml/PlayerErrorPopup.qml" line="14"/>
        <source>Playback error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlayerErrorPopup.qml" line="26"/>
        <location filename="../../src/desktop/windows/qml/PlayerErrorPopup.qml" line="30"/>
        <source>An error occurred during media playback (%1): %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlayerErrorPopup.qml" line="35"/>
        <location filename="../../src/desktop/windows/qml/PlayerErrorPopup.qml" line="59"/>
        <source>You can also try to switch to a different media format by clicking on the ⚙ symbol near the bottom right corner.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/windows/qml/PlayerErrorPopup.qml" line="40"/>
        <source>&lt;p&gt;An error occurred during media playback; this is likely caused by missing codecs.&lt;/p&gt;&lt;p&gt;Please consider installing additional video codecs from one of these sources:&lt;/p&gt;&lt;ul&gt; &lt;li&gt;&lt;a href=&quot;https://www.codecguide.com/download_kl.htm&quot;&gt;K-lite codecs&lt;/a&gt; &lt;li&gt;&lt;a href=&quot;https://github.com/Nevcairiel/LAVFilters/releases&quot;&gt;LAV filters&lt;/a&gt;&lt;/ul&gt;&lt;p&gt;Once these codecs have been installed, restart MiTubo and trying playing the media again.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistItemDelegate</name>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemDelegate.qml" line="149"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemDelegate.qml" line="158"/>
        <source>More…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistItemNew</name>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemNew.qml" line="9"/>
        <source>New list...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemNew.qml" line="16"/>
        <source>Create a new playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemNew.qml" line="25"/>
        <source>Enter the list name, e.g. &quot;Watch later&quot;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistPage</name>
    <message>
        <location filename="../../src/desktop/qml/PlaylistPage.qml" line="15"/>
        <source>Playlist %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistPage.qml" line="57"/>
        <source>Newer first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistPage.qml" line="58"/>
        <source>Older first</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistsPage</name>
    <message>
        <location filename="../../src/desktop/qml/PlaylistsPage.qml" line="15"/>
        <source>Playlists</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/PlaylistsPage.qml" line="27"/>
        <source>%n elements</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>SearchChannelDelegate</name>
    <message>
        <location filename="../../src/desktop/qml/SearchChannelDelegate.qml" line="54"/>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchChannelDelegate.qml" line="62"/>
        <source>Visit site</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../../src/desktop/qml/SearchPage.qml" line="15"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchPage.qml" line="26"/>
        <source>Search:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchPage.qml" line="32"/>
        <source>Enter search terms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchPage.qml" line="52"/>
        <source>⚙</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchPlaylistDelegate</name>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/SearchPlaylistDelegate.qml" line="33"/>
        <source>By %1 - %n elements</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchPlaylistDelegate.qml" line="44"/>
        <source>Open playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchPlaylistDelegate.qml" line="52"/>
        <source>Visit site</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextFieldWithContextMenu</name>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="16"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="22"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="28"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="34"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="42"/>
        <source>Select all</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnsupportedCodecPopup</name>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="13"/>
        <source>Unsupported media codec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="30"/>
        <source>Some or all of the media streams are using an unsupported codec. You can blacklist the codec used in the unplayable stream so that MiTubo will never try to play similar streams again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="42"/>
        <source>&lt;b&gt;Video stream&lt;/b&gt;&lt;br&gt;Codec: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="51"/>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="71"/>
        <source>Blacklisted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="52"/>
        <source>Blacklist video codec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="62"/>
        <source>&lt;b&gt;Audio stream&lt;/b&gt;&lt;br&gt;Codec: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="72"/>
        <source>Blacklist audio codec</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../../src/desktop/qml/UpdateDialog.qml" line="10"/>
        <source>MiTubo update check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UpdateDialog.qml" line="31"/>
        <source>Checking for updates…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UpdateDialog.qml" line="56"/>
        <source>An error occurred while contacting the server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UpdateDialog.qml" line="65"/>
        <source>Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UpdateDialog.qml" line="82"/>
        <source>A new version is available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UpdateDialog.qml" line="92"/>
        <source>Latest version: &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UpdateDialog.qml" line="103"/>
        <source>You are running MiTubo %1; if you obtained MiTubo from an application store, visit it to update to the latest version. Otherwise, you can download the latest version by downloading it from &lt;a href=&quot;http://mardy.it/mitubo&quot;&gt;mardy.it/mitubo&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UpdateDialog.qml" line="112"/>
        <source>Your installation of MiTubo is up to date.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoBottomRow</name>
    <message>
        <location filename="../../src/desktop/qml/VideoBottomRow.qml" line="59"/>
        <source>%1 / %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoInfoPage</name>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="15"/>
        <source>Video details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="45"/>
        <source>Failed to retrieve title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="77"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="85"/>
        <source>Watch later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="92"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="119"/>
        <source>By &lt;b&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="120"/>
        <source>By &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="128"/>
        <source>Duration: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="135"/>
        <source>Uploaded on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="136"/>
        <source>Upload date unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="143"/>
        <source>%n views</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="144"/>
        <source>View count unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="152"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="166"/>
        <source>&lt;a href=&quot;%1&quot;&gt;View web page in browser&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoSettingsPanel</name>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="15"/>
        <source>Half speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="16"/>
        <source>Normal speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="17"/>
        <source>1.25x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="18"/>
        <source>1.5x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="19"/>
        <source>Double speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="20"/>
        <source>Quadruple speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="31"/>
        <source>Streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="43"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="56"/>
        <source>Playback speed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchLaterPanel</name>
    <message>
        <location filename="../../src/desktop/qml/WatchLaterPanel.qml" line="20"/>
        <source>Add to playlist:</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/WatchLaterPanel.qml" line="36"/>
        <source>%n elements</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>YandexSearchOptions</name>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="26"/>
        <source>Published:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="34"/>
        <source>Any time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="35"/>
        <source>Today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="36"/>
        <source>Less than a week ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="37"/>
        <source>Less than a month ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="38"/>
        <source>Less than one year ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="45"/>
        <source>Duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="52"/>
        <source>Any</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="53"/>
        <source>Less than 10 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="54"/>
        <source>10-65 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="55"/>
        <source>More than 65 minutes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../src/desktop/qml/main.qml" line="92"/>
        <source>Retrieving stream information from the web page…</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
