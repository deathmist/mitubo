image: mardy/qt:xenial-qt515

cache:
  key: apt-cache
  paths:
  - apt-cache/
    # for appimage
  - tools

stages:
  - deps
  - build
  - test

variables:
  GIT_SUBMODULE_STRATEGY: recursive

.desktop_dependencies: &desktop_dependencies
  - export APT_CACHE_DIR=`pwd`/apt-cache && mkdir -pv $APT_CACHE_DIR
  - PACKAGES+=" fuse libegl1-mesa"
  - PACKAGES+=" libpulse-mainloop-glib0 libgstreamer-plugins-base1.0-0 libasound2" # for QtMultimedia
  - PACKAGES+=" gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-pulseaudio" # more Gstreamer
  - |
      apt-get update -yq && apt-get -o dir::cache::archives="$APT_CACHE_DIR" install -y \
          build-essential gettext pkg-config libglu1-mesa-dev libxrender1 \
          appstream \
          lcov gcovr \
          wget \
          $PACKAGES
  - ln -f /usr/bin/g++-7 /usr/bin/g++
  - ln -f /usr/bin/gcc-7 /usr/bin/gcc
  - ln -f /usr/bin/gcov-7 /usr/bin/gcov


build_dependencies:
  stage: deps
  artifacts:
    paths:
      - staging
  before_script:
    - *desktop_dependencies
  script:
    - STAGING="$PWD/staging"
    - mkdir $STAGING
      # Nothing here at the moment


build_desktop:
  stage: build
  before_script:
    - *desktop_dependencies
  script:
    - STAGING="$PWD/staging"
    - export PKG_CONFIG_PATH="$STAGING/lib/pkgconfig:$PKG_CONFIG_PATH"
    - mkdir build
    - cd build
    - qbs -f .. config:debug qbs.installPrefix:/opt
  dependencies:
    - build_dependencies
  artifacts:
    paths:
      - build/


test_desktop:
  stage: test
  # The appstreamcli from xenial is too old
  image: mardy/qt:bionic-qt515
  before_script:
    - *desktop_dependencies
  script:
    - cd build
    - |
        qbs resolve \
            -f .. \
            projects.MiTubo.buildTests:true \
            projects.MiTubo.enableCoverage:true
        qbs --command-echo-mode command-line \
            -p coverage
    - |
        appstreamcli validate \
            debug/install-root/opt/share/metainfo/it.mardy.mitubo.metainfo.xml
  dependencies:
    - build_dependencies
    - build_desktop
  artifacts:
    paths:
      - build/coverage-html/


appimage:
  stage: build
  only:
    - tags
  dependencies:
    - build_dependencies
  artifacts:
    paths:
      - appimage/
  before_script:
    - *desktop_dependencies
  script:
    - SRCDIR="$PWD"
    - STAGING="$PWD/staging"
    - APPDIR="$PWD/appdir"
    - OUTPUT="$PWD/appimage"
    - export PKG_CONFIG_PATH="$STAGING/lib/pkgconfig:$PKG_CONFIG_PATH"
    - LINUXDEPLOYQT_NAME="linuxdeployqt-continuous-x86_64.AppImage"
    - LINUXDEPLOYQT="$PWD/tools/$LINUXDEPLOYQT_NAME"
    - 'if [ ! -x "$LINUXDEPLOYQT" ]; then'
    -     mkdir -p tools && cd tools
    -     wget -c -nv "https://github.com/probonopd/linuxdeployqt/releases/download/continuous/$LINUXDEPLOYQT_NAME"
    -     chmod a+x "$LINUXDEPLOYQT"
    -     cd ..
    - fi
      # Remove other stuff that we don't want to be copied
    - cd $STAGING
    - rm -rf usr/bin/* usr/share/xml usr/share/doc usr/share/dbus-1
    - cd -
      # Build MiTubo
    - mkdir $APPDIR
    - mkdir abuild
    - cd abuild
    - |
        qbs -f .. \
            config:release \
            project.packageType:AppImage \
            qbs.installPrefix:/usr \
            qbs.installRoot:$STAGING
    - cd ..
      # Copy those things we need into the appdir
    - cd staging
    - cp -a --parents usr/bin/ usr/share/ $APPDIR
    - mkdir -p $APPDIR/usr/lib/gstreamer
    - cp /usr/lib/x86_64-linux-gnu/gstreamer-1.0/lib* $APPDIR/usr/lib/gstreamer/
    - |
        for UNNEEDED in \
            1394 \
            aasink \
            alpha \
            alphacolor \
            amrwbdec \
            apetag \
            asf \
            audiofx \
            audioresample \
            audiotestsrc \
            auparse \
            cacasink \
            cairo \
            camerabin2 \
            cdio \
            cdparanoia \
            coretracers \
            cutter \
            debug \
            dtmf \
            dv \
            dvdlpcmdec \
            dvdread \
            dvdsub \
            effectv \
            encodebin \
            equalizer \
            flxdec \
            gdkpixbuf \
            goom \
            goom2k1 \
            icydemux \
            id3demux \
            imagefreeze \
            interleave \
            jack \
            jpeg \
            jpegformat \
            level \
            libvisual \
            monoscope \
            mpg123 \
            multifile \
            navigationtest \
            oss4audio \
            pango \
            png \
            replaygain \
            shout2 \
            sid \
            shapewipe \
            smpte \
            spectrum \
            subparse \
            taglib \
            video4linux2 \
            videobox \
            videofilter \
            videorate \
            videotestsrc \
            wavenc \
            ximagesink \
            ximagesrc \
            xingmux \
            y4menc
        do
            rm -f $APPDIR/usr/lib/gstreamer/libgst${UNNEEDED}.so
        done
    - |
        LIBPATH="$APPDIR/usr/lib"
        for FILE in $APPDIR/usr/lib/gstreamer/*.so ; do
            ldd "${FILE}" | grep "=>" | awk '{print $3}' | xargs -I '{}' echo '{}' >> DEPSFILE
        done
        DEPS=$(cat DEPSFILE | sort | uniq | grep '^/usr/')
        for FILE in $DEPS ; do
            if [ -e $FILE ] && [[ $(readlink -f $FILE)/ != $LIBPATH/* ]] ; then
                cp -v -rfL "$FILE" "$LIBPATH/" || true
            fi
        done
        rm -f DEPSFILE
        # Also copy gio modules:
        mkdir -p "$LIBPATH/gio/modules"
        for MODULE in libgiognutls.so
        do
            cp -v -rfL /usr/lib/x86_64-linux-gnu/gio/modules/$MODULE "$LIBPATH/gio/modules/"
        done
        # Make sure we didn't copy any "forbidden" file
        P2APPIMAGE_COMMIT="5c8ea76e8df66abd49309b82eec4bdf9aad39eeb"
        wget https://raw.githubusercontent.com/AppImage/pkg2appimage/${P2APPIMAGE_COMMIT}/excludelist
        while read LIB
        do
            if [ -n "$LIB" ] && [[ ! "$LIB" =~ "^#.*" ]]; then
                echo rm -f "$LIBPATH/$LIB"
                rm -f "$LIBPATH/$LIB"
            fi
        done < excludelist

        # Remove a couple of undesired libraries of our own:
        # https://gitlab.com/mardy/mitubo/-/issues/3
        rm -f "$LIBPATH"/libxcb-dri*.so.*
    - cd ..
      # Fixup the Desktop file (https://github.com/AppImage/AppImageKit/issues/871)
    - sed -ie "s,^Version=.*,Version=1.0," "$APPDIR/usr/share/applications/mitubo.desktop"
      # Run linuxdeployqt
    - cd appdir
    - |
        LD_LIBRARY_PATH="$STAGING/lib:$LD_LIBRARY_PATH" "$LINUXDEPLOYQT" \
            $PWD/usr/share/applications/mitubo.desktop \
            -qmldir=${SRCDIR}/src/desktop/qml \
            -extra-plugins=imageformats/libqsvg.so,iconengines \
            -exclude-libs=libqsqlodbc,libqsqlpsql,libqtposition_geoclue2,libqtposition_positionpoll,libqtposition_serialnmea,libqtposition_geoclue \
            -bundle-non-qt-libs \
            -no-copy-copyright-files
    - cd ..
    - |
        # A newer is required by gio/modules/libgiognutls.so
        rm -f "$LIBPATH/libgnutls.so".* "$LIBPATH/libtasn1.so".*
    - |
        cp "${SRCDIR}/packaging/appimage/desktop-integration.sh" "$APPDIR/usr/bin/"
        rm "${APPDIR}/AppRun"
        cp "${SRCDIR}/packaging/appimage/AppRun" "$APPDIR/"
      # Remove stuff that we don't need
    - rm -rf $APPDIR/usr/qml/QtQuick/Controls{,.2}/{Universal,Imagine,Material}
    - find appdir
    - cd abuild
    - '"$LINUXDEPLOYQT" --appimage-extract'
    - 'PATH=./squashfs-root/usr/bin:$PATH ./squashfs-root/usr/bin/appimagetool $APPDIR'
      # Finally, copy the AppImage into the artifacts directory
    - mkdir $OUTPUT
    - 'mv "MiTubo-x86_64.AppImage" "$OUTPUT/MiTubo_${CI_COMMIT_TAG:-$CI_COMMIT_SHA}.AppImage"'
